\chapter{Training}

\epigraph{Responsibility and discipline must not frighten the revolutionary.}
{Nestor Makhno, \textit{On Revolutionary Discipline}\supercite{on-revolutionary-discipline}}

\index{training|(}

\noindent
Reading material and studying theory is not sufficient training to be an effective riot medic.
Hands-on and other practical training is necessary to be able to quickly and correctly render care.
Maintaining a baseline level of fitness is also recommended since riot medicine is inherently much more physical than clinical or other emergency medicine.
Training with your equipment and comrades will give you the familiarity and skill you need so that instead of focusing on the mechanics of moving with a crowd or applying a bandage, you can focus on the higher level analyses like police and Bloc movements or diagnosing an illness.

\section*{Practical Training}

Practical training is the complement to studying medical theory.
It means actually going through the mechanics of diagnosing and treating injuries and illnesses.

Practical training is necessary to quickly and correctly render care to patients.
Professionalism in care helps make patients more comfortable during treatment and will help build trust that you can be relied on to care for patients in more serious situations.
Beyond this, lacking familiarity with your equipment can inadvertently injure patients while caring for them.

\begin{figure}[htb]
\centering
\caption{Training Together\supercite{baedr}}
\includesvg[height=7cm, keepaspectratio]{medics-training-together}
\end{figure}

\index{buddy pairs!training@in training|(}
\index{medic collectives!training@in training|(}

Things you should consider practicing with a buddy or your collective are:

\begin{itemize}
    \item Bandaging different wounds
    \item Immobilizing fingers, limbs and joints (especially wrists, elbows, shoulders, and ankles)
    \item Washing pepper spray from a patient's eyes
    \item Carrying patients
\end{itemize}

Doing these with the supervision of a more experienced medic will help teach you how to correctly bandage wounds so that a protester may continue to take part in an action without ripping off a bandage or exposing their wound.

\index{medic collectives!training@in training|)}
\index{buddy pairs!training@in training|)}

\index{riot control agents!training@in training|(}

It is especially important to practice decontamination of the eyes from \gls{riot control agent}.
You will need to practice using your fingers to hold a patient's eyes open while flushing them with water.
Practice with ``live fire'' and actually spraying volunteers is incredibly useful as it will teach people who have never been sprayed before what to expect, and it simulates the actual panic and mishaps (such as medics getting pepper spray in their own eyes) you will see at actions.
It is especially important for you to practice eye decontamination on yourself so that you can flush your own eyes if necessary.
If you are the only medic, you need to be able to care for yourself so you can care for others.

\index{riot control agents!training@in training|)}

\index{certifications!training@in training|(}
\index{training!formal|(}

While it is not expected that medics receive formal training or State recognized certifications, one advantage of taking a formal course is that you will have the opportunity to practice CPR on training mannequins to get a feeling for the mechanics of chest compressions and ventilation.
Certifications may also provide legal protection for you as you practice medicine, and they may allow you to have a wider scope of practice.

Formal courses also offer practice on diagnosis where an instructor selects an illness and makes the medic diagnose what has happened, explain their reasoning, and propose treatment for the patient.
This can be done with your collective if you have medics who are experienced and comfortable leading drills of this nature.
Practicing handling, diagnosing, and treating patients is critically important, especially for medics who do not work as medical professionals.

\index{training!formal|)}
\index{certifications!training@in training|)}

\index{training!ongoing|(}

In addition, your training needs to be ongoing, even if just to refresh skills.
A 2012 study followed students who took a wilderness first aid course (the closest one can get to a riot medicine course) and tested their skill retention at 4, 8, and 12 months.
It found that there was a decrease in competency of practical skills and that a participant's self-confidence did not correlate with their actual skills.\supercite{wfa-skill-retention}
Your skills and knowledge may degrade without you realizing it.

\index{training!ongoing|)}

Training with each other in the backyard of a local squat is useful, but there is no substitute for actually doing the thing.
Experience is the best teacher, so getting out to actions will hone your riot medicine skills faster than anything else.
Going out to small, less exciting actions will allow you to practice treating patients and moving through a crowd without flying bottles and risk of arrest.

\section*{Team Building}

\index{training!team building@and team building|(}

Doing practical training with you buddy or collective is a form of team building that serves to build trust in each other's abilities as well as familiarity with working with each other.
Practicing communicating and quick decision making will help you in the field while a crowd moves rapidly or many patients need to be treated.

Many medics bring their own equipment to actions even when part of a collective, or some equipment may be shared among collective members.
Training together allows medics to familiarize themselves with the different packs and gear everyone else has.
This is useful so that during an action if someone says ``I need gauze,'' the other medics know what it looks like and where in a pack it is located.

Team building also may take place in other formats like entertaining group activities.\footnotemark[1]
Some medics have found utility out of playing board games because it is an exercise in cooperation and creative thought around strategy without becoming lost in endless hypotheticals.\footnotemark[2]

\footnotetext[1]{Sometimes called ``friendship.''}

\footnotetext[2]{
I find the board game \textit{Bloc by Bloc} to be useful for this as it is cooperative, deals with common protest concepts, and (as a bonus) is based on fighting the police.
}

However, much of this team building is not strictly necessary and serves as an aid in building trust and solidarity.
Medics who are often in the field together will naturally build a sense of unity in an organic way.

\index{training!team building@and team building|)}

\section*{Fitness}

\index{fitness|see {training, fitness}}
\index{training!fitness|(}

Taking part in an action can be physically taxing, especially if is lasts many hours or takes place in inclement weather.
The degree to which a medic becomes exhausted during the course of their duties can be lessened by increasing their fitness beforehand.
It is not a requirement that medics have fitness, nor is this meant to be a criticism of or value judgement against those who do not want to or cannot train.
Fitness is merely one tool of many in a medic's toolbox, and a small amount of fitness can be extremely advantageous.

There is no arbitrary cutoff or criteria for fitness, and lack of fitness should not disqualify someone from becoming medic.
There are simply some tasks that require a degree of endurance and strength.
An inability to keep up with a quick moving protest can make it impossible to render care if the main body outpaces the medics.
Being able to lift patients makes it possible to evacuate them.
Core strength can help prevent tiring or straining back muscles from carrying a backpack all day.

Some goals that are worth working towards are:

\begin{itemize}
    \item Using a two-person carry, carry a patient 100m
    \item Run quickly for 5 minutes
    \item Jog for 20 minutes
    \item Walk for 4 hours with a backpack without a break
\end{itemize}

If you can can accomplish these without feeling tired, cramped, or exhausted or being on the edge of muscle failure, you will have more than enough fitness to carry out your duties.
If any of these trouble you, even with as little as an hour of working out per week (one 1-hour session, or two 30-minute sessions), you may be able to  make significant improvements in a few weeks.
Light jogging plus core exercises is generally all that is necessary for training.
Hiking and bouldering are group activities that are more fun than running laps around your local park.

In general, training in the heat will build heat adaptation\index{heat adaptation!training@in training} and help prevent heat illnesses\index{heat illnesses!training@in training} during hot actions.
See \fullref{ch:heat_illnesses} for more information.

There are many ways to become more fit, so find what works for you.
Guidelines for writing a fitness plan is outside the scope of this book.

\index{training!fitness|)}

\section*{Summary}

Reading this book is not enough to prepare you for being a medic.
Practical training is strongly encouraged, and practicing diagnosing and treating patients with other medics is a skill that can only be learned by practice.
All medics, especially novice medics, are encouraged to go out to many demonstrations and use them to hone their skills.

\index{training|)}
