\chapter{Post-Action Tasks}

\epigraph{
Now we go quiet; the fight has begun.
There is a hill and I shout as I run forward: To Versailles! To Versailles!
Razoua tosses me his sword to rally the men.
We shake hands at the top; the sky is on fire, and no one has been wounded.
}{Louise Michel, \textit{Mémoires}\supercite{memoirs-louise-michel}}

\noindent
Your duties as a medic are not done when you leave an action or even when you get home.
Immediately following an action is the best time to start preparing for the next while things are still fresh in your head.
A very small amount debriefing and preparation can drastically reduce the amount of work you have to do before your next action.
Taking the time to talk with comrades can also help with your mental health and the mental health of your comrades.
For more on psychological first aid, see \fullref{ch:psychological_care}.

\section*{Buddy Check-In}

\index{buddy pairs|(}

At the end of the action, check-in with your buddy.
This may be as simple as asking ``Everything good today?''
If there were traumatic injuries, violence, or other stressful events during the day, try to talk about it with your buddy, but don't force a conversation they do not want to have.

Additional information on the kind of care you can provide for one another following traumatic events can be found in \fullref{ch:psychological_care}.

\index{buddy pairs|)}

\section*{Debriefing and Retrospective}

\index{debriefing|(}
\index{retrospectives|(}

Shortly after the action, or in the days following, your collective or working group may want to hold a longer discussion about the action.
This is sometimes called a debrief or retrospective.
Retrospectives help individuals and groups identify successes as well as things that could have been done better so that future actions can be handled with fewer mistakes.

If you are part of a multi-day action, you should consider holding short debriefs after each day so that improvements can be made for the following day.

An important part of the retrospective is to ensure that they are blameless.
Many times the perceived failings of an individual were due to systemic failings of the collective at large.
If someone was in the wrong position and got kettled, was this because of poor planning or bad communication?
The purpose of the retrospective is not to ``name and shame,'' but to understand the root cause of an error so that it may be avoided in the future.

It's useful to do a round where all members quickly say what they felt went well, was difficult, and what could be improved.
This is followed by an open discussion about how the things that went well can be repeated as well as how to restructure the collective or plans so that errors are not repeated.

Holding retrospectives that are friendly and non-adversarial helps build trust among members of the collective\index{medic collectives} or working group\index{working groups}.

\index{retrospectives|)}
\index{debriefing|)}

\section*{Community Coordination}

If there is legal\index{legal support} or anti-repression\index{anti-repression} support in your region, you may want to contact them to report your findings.
This may mean the names of arrested comrades who need jail support\index{jail support}, or it may mean reporting aggregate counts of injuries for publishing.

\section*{Emergency Contacts}

\index{emergency contacts|(}

If any emergency contacts are waiting on you, make sure you contact them to let them know you are home safe.
If you are anyone's emergency contact, consider reaching out to them to check if they made it home safely.

\index{emergency contacts|)}

\section*{Decontamination}

\index{riot control agents!decontamination|(}

After you have gotten home, you may need to fully decontaminate all your equipment from riot control agents (\acrshort{RCA}s).
Full instructions can be found in \fullref{ch:rca_contamination}.
It is important to do this immediately to minimize the spread of RCAs into the rest of your home.
Failing to immediately decontaminate may lead to you using contaminated equipment during your next action.

\index{riot control agents!decontamination|)}

\section*{Equipment Check}

When you get home from an action, you should at a minimum you make a list of all the supplies and equipment you will need to restock before the next action.
List all single use supplies that were used up during the action such as gauze, bandages, or batteries.
Additionally, list any supplies that were damaged of confiscated.

As mentioned before, if \glspl{riot control agent} were deployed during the action, you will need to decontaminate your bag, its contents, and your clothes.

\begin{figure}[htbp]
\caption{Respirator Cleaning\supercite{baedr}}
\centering
\begin{subfigure}[b]{0.45\textwidth}
    \centering
	\includesvg[width=\textwidth, height=4cm, keepaspectratio]{mask-cleaning-remove-filter}
	\caption{Remove Filters}
\end{subfigure}
\begin{subfigure}[b]{0.45\textwidth}
    \centering
	\includesvg[width=\textwidth, height=4cm, keepaspectratio]{mask-cleaning-wipe-down}
	\caption{Wipe Down}
\end{subfigure}
\end{figure}

If your helmet or eye protection was struck with anything, check it for damage.
Helmets are often designed for only one impact, and they may have significantly degraded protection once struck.
If you have a full face respirator, check the visor for cracks and other damage.

If you have replacements for anything that was used during the action, restock your bag.
If your bag became disorganized during the action, reorganize it.
Plug in any devices that need to be recharged.

Eyewash bottles and water bottles should be emptied, cleaned, and left to dry for the next action so that mildew does not grow in them.

\section*{Decompression}

\index{self-care|(}

After you have cleaned up, don't forget to take care of your body and mind.
Eat, hydrate, and relax.
You may want to stretch after a long day to help alleviate muscle tightness.
You may find yoga or meditation calming, or you might want to watch movies with friends or go for a swim.
You know what you enjoy and find relaxing, and it is important to take care of yourself too.

\index{self-care|)}

\section*{Summary}

Preparing your equipment for the next action immediately following an action is useful because it makes it easy to grab your bag and run should something spontaneous happen.
Taking care of other loose ends and decompressing allows you to mentally close out the event and move on to your life outside of being a medic.
