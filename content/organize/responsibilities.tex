\chapter{Responsibilities}

\epigraph{Most of my heroes are ordinary. /
Their shapes, forms, and sizes, they may vary. /
And some are just like you, and some are like me. /
Some of my heroes are ordinary.}
{Shireen Crisis Folk, \textit{Heroes}\supercite{shireen-crisis-folk-demo}}

\index{medics!responsibilities|(}

\noindent
Medics make up the autonomous part of the healthcare system that attends actions and interfaces with the organizations and individuals involved in protest and civil unrest.
Even during the least confrontational actions, participants may need medical assistance.
Hot weather can make people ill, and fascists may make surprise attacks against participants.
As the level of violence increases, police repression\index{repression} can make it increasingly risky for protesters to use traditional \gls{emergency medical services} (\acrshort{EMS}),\footnotemark[1] so social movements must provide their own medical care.

\footnotetext[1]{
During the 2019--20 Hong Kong protests\index{Hong Kong!2019--20 protests}, injured protesters were afraid of being arrested at hospitals.\supercite{hong-kong-police-repression-hospitals}
At times, police road blocks prevented patients from getting to hospitals at all.\index{police!interference with medics}
Medics set up two hotlines, one for physical trauma and one for emotional trauma\index{psychological first aid!hotlines}, to help medical professionals to provide aid to protesters.
}

While rare, life-threatening injuries require immediate attention, and survival rates drop with each additional minute before aid arrives.
Medics who are on scene can administer lifesaving interventions until the patient can be brought to advanced medical care.
In some cases, advanced medical care may be delayed by large crowds, road blocks, or malicious police obstruction; or it may not be available at all.

\index{mutual aid|(}

The term ``propaganda of the deed''\index{propaganda of the deed} is often assumed to refer to violence alone, but it means so much more.
By helping each other, we encourage others to do the same.
In a 2007 essay, the Aftershock Action Alliance\index{Aftershock Action Alliance} described such direct challenges to the State as ``insurrectionary mutual aid.''\supercite{insurrectionary-mutual-aid}

\begin{displayquote}
It is through acting and learning to act that we will open a path to insurrection\ldots

The force of an insurrection is not the state’s military response, but the social upheaval it generates.
Beyond the surface of the armed clash, the importance of any particular revolt should be evaluated by how it managed to expand the paralysis of normality in a given area and beyond.
\end{displayquote}

In this sense, caring for one another demonstrates the fallibility of existing institutions and that we are capable of providing what the State cannot or will not.
Our actions provide the means for reshaping and replacing the existing order.

\index{mutual aid|)}

\section*{Tasks}

Medics' responsibilities do not begin and end with an action.
There is preparation and coordination that needs to be done with organizers and participants beforehand.
Ongoing care and jail support\index{jail support} may need to happen after actions.

Your responsibilities during actions are something you are likely already familiar with.
They may include:\supercite{street-medic-handbook-v1}

\begin{itemize}
    \item Providing care to injured individuals
    \item Spreading calm when others may panic
    \item Evacuating injured persons to safety
    \item Interfacing with traditional EMS
    \item Setting up makeshift clinics to care for multiple casualties
    \item Instructing patients on aftercare
    \item Providing emotional and psychological support for patients experiencing trauma
\end{itemize}

Outside of actions, you will need to work to prepare yourself and others for medical emergencies that may occur in the future.
Some of these topics will be covered more in depth as you read this book.

\index{education|(}
\triplesubsection{Educate yourself}
Reading this book and practicing medicine in the field is not enough to be a proficient medic.
You should supplement the knowledge in this book through courses (formal or informal) with experienced educators.
\index{education|)}

\index{tactics|(}
\triplesubsection{Study tactics}
Study the tactics and practices of other street medics and stand upon the shoulders of giants.
Study the behavior and tactics of police and fascists in you region.
Prepare yourself for the confrontations you may encounter.
\index{tactics|)}

\index{training|(}
\triplesubsection{Train}
Train you body and mind with other medics.
Work out together.
Run drills together.
Become adept in all areas of riot medicine.
\index{training|)}

\triplesubsection{Coordinate with others}
Learn to work with other organizations in your region.
There may be legal observers\index{legal observers}, anti-repression\index{anti-repression} support, and other organizations that provide mutual aid during actions.
Legal support may document injuries to file official police brutality complaints.
Medics can catalogue injuries and turn anonymized documentation\index{documentation!injuries@of injuries} over to legal teams as part of lawsuits against the police.

\triplesubsection{Learn the local languages}
There may be one primary language in your region, but not everyone may be able to speak it well.
Minorities, immigrants, and refugees have more reason to protest and are more likely to targeted by police brutality or fascist aggression.
Learning the basics of other common languages will make it easier to calm and treat patients.

% TODO this section should also include educating others pre-action (infographics about hydration, pepper spray, cold, etc.)
\index{training!others@of others|(}
\triplesubsection{Educate others}
It is your responsibility to educate the next generation of medics.
As you learn, teach others.
Teach non-medic activists the basics of first aid and treatment for riot control agents so they can care for themselves.
A movement or collective that is open and shares will inspire others to join it.
By educating others, you will help prevent injuries and minimize lasting damage when medics aren't present.
\index{training!others@of others|)}

\index{jail support|(}
\triplesubsection{Provide post-arrest jail support}
Arrested\index{arrest} comrades may be injured while being arrested.
They may have psychological trauma from police abuse.
Be ready to pick them up after they've been arrested.
All you need is a medic bag, some food, water, and clean clothes for them to change into.
\index{jail support|)}

\section*{Limitations}
\index{medics!limitations|(}

There are some limitations all medics must adhere to while operating in a medic capacity.

\index{donations|(}
\triplesubsection{Offer free service}
Medics must not charge for their services.
They must not solicit donations while in service.\footnotemark[2]
These solicitations may be interpreted as mandatory ``donations'' that must be given up in order to receive care.
The very act of charging for services removes the solidarity from the care provided and turns mutual aid into a transaction.
Charging for service creates a hierarchy among patients where only those who can pay may receive care.
\index{donations|)}

\footnotetext[2]{
Passive solicitation by clipping a can to the outside of you bag is, of course, entirely reasonable.\index{donations}
}

\index{patient-caregiver confidentiality|(}
\triplesubsection{Maintain patient-caregiver confidentiality}
Medics must not break patient confidentiality.
Apart from the ethical concerns of violating the trust between a medical practitioner and their patient, this may be illegal\index{legality} and violate local healthcare regulations.
Medics do not talk to the police or journalists\index{journalists!patient privacy@in patient privacy} about their patients.
Medics do not share photos or blog\index{medics!blogging@and blogging} about patients they treat because given the number of cameras present during actions, it is unlikely this can be done while maintaining patient anonymity.\footnotemark[3]
\index{patient-caregiver confidentiality|)}

\footnotetext[3]{
Exceptions to this are when things are done with consent of the patient or when it is sufficiently anonymized.
Documenting\index{documentation!injuries@of injuries} police brutality can be useful propaganda\index{propaganda} when done well, but it should not come at the expense of the patient's anonymity or safety.
The general rule is do not publish photos or details of care rendered for the sake of notoriety or proof of importance.
}

\index{consent|(}
\triplesubsection{Obtain informed consent}
Medics must get informed consent from a patient before touching them or beginning care.
When a patient consents to care, they are only consenting to the care necessary to heal them or provide lifesaving interventions.
They are not consenting to a full medical examination or the collecting of tissue samples.
Patients cannot consent to procedures like this because, as someone who is in need of care or in a possibly altered mental state, their consent is not freely given.
Asking to take samples is likely coercion.\footnotemark[4]
Additional details about consent during an examination can be found in \fullref{ch:patient_assessment}.
\index{consent|)}

\footnotetext[4]{
\index{France!Movement des Gilets Jaunes|see {Mouvement des Gilets Jaunes}}
\index{Gilets Jaunes|see {Mouvement des Gilets Jaunes}}
\index{Yellow Vest Movement|see {Mouvement des Gilets Jaunes}}
\index{Mouvement des Gilets Jaunes|(}
\index{consent|(}
\index{France|(}
During Yellow Vest actions in France in 2019, there were reports on April 20th and May 1st\index{May Day} that street medics were taking blood samples.\supercite{paris-medics-blood-samples}
These medics claimed to be doctors who wanted to analyze blood samples for hydrogen cyanide\index{hydrogen cyanide!CS gas@in CS gas} from expired CS gas\index{CS gas!expired}.
Consent was given, but not freely, while injured persons were treated on the streets.
Photos taken of these events show medics wearing respirators and patients attempting to cover their faces because they were still in danger.
The medics chose to treat the patients as lab rats to satisfy their own curiosity instead of getting the patients to safety.
\index{France|)}
\index{consent|)}
\index{Mouvement des Gilets Jaunes|)}
}

\index{scope of practice|see {medics, scope of practice}}
\index{medics!scope of practice|(}
\triplesubsection{Stay within your scope of practice}
Medics must not provide care outside their \gls{scope of practice}.
A medic must not use equipment they do not know to use safely or correctly, and they do not administer treatments they are not qualified to give.
Additionally, there may be laws\index{legality} that forbid use of certain equipment, medications, or procedures without certification.
\index{medics!scope of practice|)}

\index{patient abandonment|(}
\triplesubsection{Do not abandon patients}
Medics must not abandon a patient once they have begun care.
Generally, this means continuing care until treatment is completed, a more qualified provider takes over, the patient's illness is outside the medic's scope of knowledge, or the scene becomes unsafe.
Further details can be found in \fullref{ch:patient_assessment}.
\index{patient abandonment|)}

\index{medics!limitations|)}

\section*{Risk and Self-Preservation}
\index{medics!risk|(}
\index{medics!self-preservation|(}

Confronting the State and fascism is an inherently risky endeavor, and there is some amount of risk you need to accept by joining a social movement.
You should make preparations in case you are hospitalized or arrested.
This is covered further in \fullref{ch:pre_action_planning}.

\index{police!interference with medics|(}
When treating a patient for any serious injury, you should not stop unless law enforcement makes you stop.
By becoming a medic and offering treatment, this is a risk you accept.
Obeying police and halting treatment can kill people.\footnotemark[5]
If police want you to stop, make them drag you away from your patient.\footnotemark[6]
\index{police!interference with medics|)}

\footnotetext[5]{
\index{police!interference with medics|(}
During Unite the Right\index{Unite the Right rally} in Charlottesville, Virginia\index{Charlottesville, Virginia} on August 12th, 2017, neo-nazi James Alex Fields Jr. drove a car into antifascist protesters injuring, among others, Heather Heyer.
Street medics assisted Heyer and performed CPR when she went into cardiac arrest.
A State Trooper forcibly removed an EMT from assisting, threatened others who lined up to help, and attempted to stop a street medic (a nurse) from performing CPR.\supercite{a12-medic-igd, a12-medic-tweet}
Heyer died as a result of her injuries.
\index{police!interference with medics|)}
}

\footnotetext[6]{
This shouldn't be taken as literal in all circumstances.
If a patient has a small scrape, it might not be worth it to get arrested.
Conversely, a grievously injured patient may need to be abandoned if police show up and the consequence of getting arrested is getting disappeared.
Protect your patient, but also use your own judgement.
}

\index{legality|(}
Generally, you should know what laws you are willing to break in the course of your work.
This may mean simply remaining with a protest that is declared an unlawful assembly, or it may mean working closely with insurrectionaries during fighting in the streets.
You should figure out what actions you are or are not willing to take or what laws you are willing to break before you are confronted with the decision.
This allows you to have a sense of risk beforehand so you don't make irrational snap decisions you may later regret.
\index{legality|)}

\begin{figure}[tb]
\centering
\caption[French Medics Hiding From Riot Cops]{French Medics Hiding from Cops\supercite{snailsnail}\textsuperscript{,}\footnotemark[7]}
\includesvg[width=\textwidth, keepaspectratio]{french-medics-hiding}
\end{figure}

\footnotetext[7]{
\index{Mouvement des Gilets Jaunes}
On January 11th, 2019, this photo\supercite{pedro-fonseca} was taken in Paris showing medics and journalists hiding around the corner from riot cops.
}

\index{riot tourism|(}
More common than medics who are excessively risky are medics who are unwilling to take any meaningful risk in the course of their work.
There are people who don a medic's uniform only as a means of getting closer to conflict, using the uniform as a shield against arrest or police brutality.
Sometimes these people are capable of performing basic first aid, and other times they are not.
They may genuinely believe themselves to be medics, but often they are incapable of providing aid and leave patients behind.\footnotemark[8]
These people are riot tourists.\footnotemark[9]
They participate in mass actions so they can get close to the action to say ``I was there,'' and regale their friends with stories of their bravery while facing far less risk than the other protesters.\footnotemark[10]
Don't become a medic for the safety.
Don't become a medic for the stories you can tell.
Become a medic to help others.
\index{riot tourism|)}

\footnotetext[8]{
In 2018, at the start of the Yellow Vest movement\index{Mouvement des Gilets Jaunes}, there was an increase in the number of street medics and street medic groups.
Many of these groups were inexperienced and lacked the medical knowledge necessary to render care.
Local medics suspected that in part this influx was due to the relative safety medics had compared to other protesters.
}

\footnotetext[9]{
Not to be confused with the same term referring to individuals who travel to have the opportunity to participate in mass actions, though in some cases they may overlap.\index{riot tourism}
}

\footnotetext[10]{
Not unlike some so-called ``journalists.''
}

Just as much as your have a responsibility to not shy away from risk, you have a responsibility to your own self-preservation.
If you get injured at an action, not only can you not help others, but precious resources must be diverted towards helping you.
You may become so injured that you cannot help at any action for months.
Likewise, if you are arrested, you cannot help anyone.

Activists, including riot medics, may develop unhealthy complexes that push them into engaging in unnecessarily risky behavior.
Some people develop martyr complexes\index{martyr complexes} where they feel they aren't really contributing toward a cause unless they are suffering.
This may come from external drivers like the need to ``prove'' to others that they are truly dedicated to a cause or the need to demonstrate credibility by getting injured or arrested.
The best way to demonstrate your dedication to a cause is by repeatedly showing up to actions and efficiently rendering care.

\index{medics!self-preservation|)}
\index{medics!risk|)}

\section*{Summary}

As a medic, you should be calm, knowledgeable, and willing to take whatever steps are necessary to provide care.
You should practice mutual aid and encourage it in others.
Medicine is a specialty skill, and you should teach others so that they can help with the burden of supporting other actions and treating the injured.

\index{medics!responsibilities|)}
