\chapter{General Tactics}
\label{ch:general_tactics}

\epigraph{There can be no separation of the revolutionary process from the revolutionary goal.}{Murray Bookchin\supercite{forms-of-freedom}}

\index{tactics!general|(}

\noindent
In order to be an effective medic, it is not enough to simply learn medicine and have a well stocked bag.
A medic must learn how to operate tactically during an action as well as within the political landscape of their region.
In the field, doctors who are used to practicing medicine in a clinical environment may find themselves overwhelmed and ineffective when forced to work in a hostile and chaotic environment.
Medics who draw the ire of the police or alienate allies may find that they are hindered in their ability to provide medical care for injured comrades.

The following chapters will discuss the tactics medics can use to effectively care for patients so that they can survive not just a single action but continue to operate over months and years.
The tactics that will be discussed may not apply to you, and by attending a single action in your region, you will gain more specific knowledge than can be given here.

\section*{Neutral vs. Partisan}

Left-wing protesters have two primary opponents.
First are the police who break strikes, evict occupations, and beat the left clear a path for nazis.
Second are the nazis, racists, and nationalists themselves.\footnotemark[1]
Depending on your region and the competency of local law enforcement, one may be a greater threat to the left than the other.
In some regions, police show up with overwhelming force to keep opposing factions separate, and a left-wing counter-protect must first fight their way through police lines before they can fight fascists.
In other regions, police will either allow the left and the right to fight so they can pick off ``violent anarchists'' for arrest\index{arrest}, or they are genuinely incapable of enforcing anything resembling so-called order.
Of course, there is significant overlap in ideology between the two broad categories, and the police may explicitly coordinate with and protect right-wing extremists.

\footnotetext[1]{
And war-mongering neoliberals, among others.
}

As a medic, there are many tactics you and your collective can use to avoid police violence so that you may work unimpeded.
This principally relies on outwardly appearing to be a neutral medic rather than a partisan protester.
This neutrality of actions can also shield you from right-wing vigilante violence while you are on the streets, though this protection is far less effective and reliable.\footnotemark[2]
To the extent it works, in a street brawl, one must pick out either the biggest threat or the weakest target, and medics are neither.\footnotemark[3]
Thus, this section focuses on how one can use the appearance of neutrality as a countermeasure to the State's punitive measures.

\footnotetext[2]{
German nazis attacked kettled medics who were treating patients during the right-wing rioting in Chemnitz in 2018.\supercite{chemnitz-medics}
US nazis attacked and maced a medic who was treating a stab wound during the \acrshort{TWP} rally in Sacramento, California in 2016.
}

\footnotetext[3]{
This is a simplification, and medics may face fascists on the street in other ways beyond in street brawls.
For example, being followed home after an action.
}

\subsection*{Medic Classes}

\index{medics!terminology@terminology for|(}

Traditionally, medics tend to divide themselves and others into two broad classes based on attire and tactics during an action.
In Germany\index{Germany} the terms red medic and black medic are used to distinguish these neutral and partisan medics, and in France\index{France}, the terms rescuer and street medic have similar meanings.\footnotemark[4]
Based off my own experiences, there is some middle ground between these two categories that is worth differentiating.

\footnotetext[4]{
How common these are, I cannot say.
Medics from those regions have told me this, and I assume it to be true.
}

Medics can be divided into the following three classes: uniformed medic, street medic, and black medic.\footnotemark[5]
These aren't hard categories, and the tactics and attire may include some from other classes, but they approximately map to how medics are perceived by the police and media.

\index{medics!terminology@terminology for|)}

\footnotetext[5]{
These are not widely used classifications, though they may be informally used by different groups.
I had to come up with some convenient language for this chapter.
}

\begin{figure}[htbp]
\caption{Medic Attire\supercite{snailsnail}}
\centering
\includesvg[height=7cm, keepaspectratio]{medic-attire}
\end{figure}

\subsubsection*{Uniformed Medic}

A uniformed medic is a medic who dresses in an \acrshort{EMS} uniform and refrains from active participation in an action.
To someone on the sidelines, a uniformed medic should not appear to be a protester.

\index{clothing|(}
\triplesubsection{Attire}
The most important factor defining a uniformed medic is, as the name suggests, the uniform.
No amount of neutral actions will deflect police violence unless the uniform is nearly indistinguishable from the region's EMS.
Uniformed medics should wear high-visibility pants, vests, and jackets with reflective strips.
These are typically in neon orange, neon yellow, or red.
An EMS backpack is helpful for completing the uniform, but it is not required.

There is significant difference in quality for high-visibility attire.
The sort of of jackets and vests worn by cyclists or required to be kept in cars for emergencies look cheap and unprofessional, and this is obvious to most observers.
Jackets and vests worn by EMS and construction workers are clearly more professional.
While wearing them, to even a trained eye, you will be indistinguishable from traditional EMS.
If you are part of a collective, acquiring matching or nearly matching uniforms heavily adds to the apparent professionalism and helps with this appearance of neutrality.

If you carry a helmet, it should ideally white, red, or in the colors of your region's EMS.

Medics are discouraged from using white lab coats as part of their uniforms.
In many regions, these are strongly associated with or even reserved exclusively for physicians.
Wearing one may be misleading.
Blood also makes obvious stains against white fabric which make increase panic during an emergency.
Many lab coats are not resistant to penetration by liquid.
Scrubs are, however, entirely acceptable.

\index{clothing|)}

\triplesubsection{Actions}
To maintain the appearance of neutrality, a uniformed medic does not actively participate in actions.
This includes not wearing political symbols, not carrying banners, not chanting, and not actively engaging against fascists or the police.
They may need to walk next to or behind a demo in order for the police to treat them as neutral.
Unless there is widespread civil unrest, uniformed medics cannot fight cops, and unless they are attacked first, they cannot fight fascists.

If a collective has online or social media presence, they may need to pick a neutral name and use overly neutral language to help maintain this illusion.
This may mean picking a name like ``Metropolis Action Medical'' over a name like ``Metropolis Anarchist Street Medics.''
They may also need to keep the content of their posts neutrally toned.
For example, they may be able to write posts critical of the police, but they should refrain from ending every post with \acrshort{ACAB}\index{cops!all are bastards} and \acrshort{FTP}\index{police!fuck the}.

\index{police!fuck the|seealso {cops, all are bastards}}
\index{cops!all are bastards|seealso {police, fuck the}}

\triplesubsection{Advantages}
Uniformed medics are generally targeted by police less than other medics.\footnotemark[6]
They may be able to cultivate a cordial relationship with the police which may allow them to cross police lines and assist those who have been arrested.
They may also be able to cross police lines to get to different parts of an action, and they may be able to enter and exit kettles.

\footnotetext[6]{
This does vary somewhat by region, and it seems to be the least true in the US where few (if any) medics use this tactic.
}

Generally, uniformed medics are not searched, and, if so, not as heavily as other protesters.
They are the least likely to have equipment confiscated.
They also may be allowed to bring additional personal protective equipment to actions when it is forbidden for protesters to wear such equipment as it may qualify as a ``passive weapon.''\footnotemark[7]

\footnotetext[7]{
\index{legality|(}
There was a case of this protection for medics being challenged in Germany\index{Germany}.
Following the arrest\index{arrest!medics@of medics} of a medic in 2016, a legal battle ended in 2018 that ensured that medics could carry their helmets.\supercite{german-medics-helmets, german-medics-helmets-interview, german-medics-helmets-amnesty, german-medics-helmets-resolution}
If German medics didn't have an established neutral presence during actions, it possible they would have lost these protections.
\index{legality|)}
}

Uniformed medics can use themselves (more specifically, their neutrality) as shields to protect protesters from police violence.
By standing in the way, they may be able to deter police charges or use of riot control weapons.\footnotemark[8]
In part, this depends on the presence of journalists\index{journalists}\footnotemark[9] who can capture police assaulting neutral medical support.
Similarly, a uniformed medic refusing to stop rendering care to a protester and subsequently getting arrested can be powerful propaganda\index{propaganda}.

\footnotetext[8]{
For an interesting example of this, consider the case of the 60-year-old Martin Friedrich Bühler who used his appearance as a clean-cut, old white guy to interrupt violence during the G20 riots in Hamburg in 2017.\supercite{g20-martin-buehler-vice}
It should be noted, however, that he does not have explicitly left-wing views (as suggested by some\supercite{g20-martin-buehler-queer-anarchism}).
Much of what has been written about him\supercite{g20-martin-buehler-volksrant} has been distorted by by a game of telephone leading to Bühler having mythic and more radical characteristics.
In his own words, Bühler opposed violence on both sides.\supercite{g20-martin-buehler}
Regardless, the key take away is that police understand the poor optics of attacking those society perceives as neutral or protected.
}

\footnotetext[9]{
There are places in this book where I talk shit on shoddy journalists, but I truly believe that quality journalism is immensely important for social movements and antifascism.
}

Those who are less radical and new to protesting and social movements tend to place greater trust in uniformed medics.
Fully uniformed medics are the most easy to identify in a crowd.

The appearance of neutrality can be used to form tactical alliances with various appendages of State apparatus to turn it against itself.
For example, police watchdog groups and \acrshort{MP}s may be more willing to offer legal support or publish documented cases of police brutality from a neutral group rather than a partisan group.
Say what you will of such alliances, but having a \acrshort{MP} who can keep police off medics' backs can be useful.

\triplesubsection{Disadvantages}
The appearance of neutrality is difficult to maintain, and if this image is shattered, uniformed medics may again become impeded in their ability to provide care.
It can be frustrating to not be able to de-arrest comrades, and it can be painful to have to stand idly by until police violence ends before you can begin care of patients.
Polite professionalism with the police can be off-putting to members of your movement and participants of the protest.

If it is not common in your region to wear EMS attire, you may appear out of place and may arouse suspicion as you may appear to be a collaborator or agent of the State.
You may also seem wildly uninformed about the nature of social movements in your region.
There are also some actions where a uniformed medic comes across as overkill, and acting as a street medic makes more sense.

Another disadvantage of a uniform is a legal concept that goes by names like ``scene safety.''
In short, this is the concept that prevents rescue personnel from being required to enter actively hostile situations until law enforcement can ensure that medical personnel will not harmed by dangerous persons.
Scene safety is (generally) the legal concept that protects medical personnel from legal liability if they do not provide aid in such cases.
Law enforcement may be able to prevent you from entering an action if you are dressed in EMS attire even if you are neither certified nor a medical professional.
Even if this is a concept in your region, it may be enforced at the whim of any given officer, so this may not always apply.

Less a disadvantage, but more a consideration is that fully uniformed medics are expected to have greater medical competency.
Being unable to treat a patient or slightly panicking during treatment will seem grossly incompetent where as similar actions from other medics is more expected.
Anecdotally, this can erode trust in medics' ability to provide medical care.

\triplesubsection{Misuses of neutrality}
Some medic groups will provide aid to police and fascists, sometimes because they believe that medics have a moral obligation to be ``truly'' neutral, and other times with some vague hand-waving in the direction of the Hippocratic Oath.
It will make it difficult for others to trust such medics, and, more importantly, it uses up finite time and resources that are better used for helping allies instead of enemies.
Medics who do this are often derided by elements of the left as being apolitical and only attending riot to get up close to the action without caring about goals or ideals of any of the sides.

Others have said that they use these ``true neutral'' tactics as they feel this is the only way they can operate safely, and that it is needed to maintain the shield of neutrality.
Choosing neutrality in this sense as a way to minimize risk is failing to show solidarity to those facing the brunt of State and police repression\index{repression}.
Do not become a State collaborator to protect yourself.
Do not throw others under the bus.

\subsubsection*{Street Medic}

A street medic is an active protester.
They are the ``classic''  and prototypical idea of a medic at a protest.
Their differentiation from other protesters is that their focus is on providing care rather than actively confronting opposition.
To someone on the sidelines, a street medic is a protester but not the main aggressor.

\begin{figure}[htbp]
\caption{Medic Symbols}
\centering
\begin{subfigure}[b]{0.3\textwidth}
    \centering
	\includesvg[width=0.8\textwidth, height=4cm, keepaspectratio]{anarchist-black-cross}
	\caption{Black Cross}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
    \centering
	\includesvg[width=\textwidth, height=4cm, keepaspectratio]{red-cross}
	\caption{Red Cross\footnotemark[10]}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
    \centering
	\includesvg[width=\textwidth, height=4cm, keepaspectratio]{star-of-life}
	\caption{Star of Life\footnotemark[11]}
\end{subfigure}
\end{figure}

\footnotetext[10]{
Use of the red cross, red crescent, and related logos may be trademarked by your regional branch of the International Red Cross and Red Crescent Movement.\supercite{red-cross-germany}
Their use without permission, even by unaffiliated medics, can lead to large fines.
This is yet another reason to hate that organization.
To avoid legal problems, some medics use green or blue crosses to identify themselves.
Of course, you could always just use a black cross.
}

\footnotetext[11]{
The symbol within this version of the star of life is the Rod of Asclepius, a single serpent wrapped around a rod.
This is often confused with the caduceus, a winged staff with two intertwined serpents.
The caduceus is staff held by Hermes that symbolizes commerce.
}

\index{clothing|(}
\triplesubsection{Attire}
Street medics do not dress in EMS uniforms.
They may still wear high-visibility vests, but more often, they wear t-shirts or jackets with large medic logos, either screen printed or as patches.
They may wear armbands or helmets with medic logos.
In short, the attire is often ``street clothes, but with red crosses.''
\index{clothing|)}

\triplesubsection{Actions}
Unlike a uniformed medic, they can chant, carry banners, and display political symbols.
They should generally refrain from actively attacking the police as this can taint police perception of all medics which may endanger others who attempt to rely on neutrality.

\triplesubsection{Advantages}
The lack of self-imposed restriction on behavior feels more liberating and doesn't feed the cop in your head.
Being able to intervene to de-arrest comrades or to help protesters may lead to less harm to protesters than acting neutral and only providing care after an injury has happened.
Street medics are also not usually subject to scene safety restrictions.

\triplesubsection{Disadvantages}
As a protest turns into a riot, street medics may lose the protections neutrality affords faster than uniformed medics.
Likewise, this protection disappears as prolonged protest turns into mass unrest (such as in France\index{France} with the Yellow Vests\index{Mouvement des Gilets Jaunes}).

\subsubsection*{Black Medic}

A black medic is a medic who has no or minimal markings and carries a small medic bag to assist with injuries.
Black medics may be part of a affinity group or embedded in the Black Bloc\index{Black Bloc} to assist injured persons in kettles.
They may also be stealth medics in plain clothes and not look like someone associated with the action.
There are no self-imposed restrictions on a black medic's behavior.

\index{clothing|(}
\triplesubsection{Attire}
Black medics should not dress in a way where they would be obviously considered to principally be medical support by an outside observer.

Black medics can regain some protection from police violence by keeping a light-weight, high-visibility vest in their medic bag.
Donning this before beginning treatment of a seriously injured protester may help reduce the risk of police interference, and it will make it easier for other medical help to spot you in a crowd as they come to assist.
\index{clothing|)}

\triplesubsection{Actions}
There is no restriction on the actions a black medic can take, nor any recommendation.

\triplesubsection{Advantages}
This is the easiest class for new medics as there is the least expectation of care and professionalism, and it requires the least amount of gear, both medical and clothing.
Black medics can embed themselves with the Black Bloc\index{Black Bloc}, and this allows them to provide support for radical actions where other medics would be too obvious.
They can be directly on the front line and provide instant medical support in cases where other medics would need to stay further back to avoid being in direct fire or classified as ``rioters.''

\triplesubsection{Disadvantages}
Black medics have no protection against police violence relative to other protesters.
They may be heavily searched on the way to actions and have their equipment confiscated.
Thus, they tend to be the least well equipped which can inhibit their ability to provide care.
Further, because they are the least identifiable (or not identifiable at all) as medics, they must make the most active effort to seek out injured protesters.

\subsection*{Choosing a Class}

Some medics may chose to operate under the cover of neutrality in order to maximize their effectiveness in providing medical care.
They laud neutrality as realpolitik because it takes into account the fact that liberal democracies are more averse to radical displays of politics.
Others find that neutrality is itself subversive because it leverages the concept of decency against the State that it is fighting.
Some may oppose a lack of neutrality because it blurs the line between medic and ``combatant'' which may put medics at risk.

Those who reject the appearance of neutrality may be claiming ideological purity on the grounds that dressing in a way the State and masses find appealing is a concession to the superiority of the status quo.
They may do so simply because they find street clothes more comfortable to don't want to spend money on a uniform when they could divert those funds to other more useful medic gear.
Some medics will openly display their political leanings and refuse to allow themselves to be seen as neutral.\footnotemark[12]
Others disagree with the concept of neutrality as a capitulation to the State and a recuperation of radical politics.
They may claim that rigid neutrality reinforces the idea that protesters are neatly divided into ``good'' (peaceful) and ``bad'' (violent).

\footnotetext[12]{
For there is no such thing as neutrality in the fight against fascism.
}

This division between neutral and partisan is not entirely clear as the meaning is subjective to both members of the public as well as the police, and what constitutes neutral may be a moving or even unattainable goal.
Some medics may find success in wearing uniforms and acting neutral.
Others may find this has no effect on their ability to avoid police repression\index{repression}.
This is something you must figure out for yourself as it varies by region.

At some point, this shield of neutrality will crumble.\footnotemark[13]
As protest turns to riot, and continuous rioting turns to insurrection, the care the State has for maintaining even a semblance of a Lockean social contract will disappear.
How fast this happens or to what degree will vary, and to rely on neutrality for indefinite protection is a sure way to get arrested\index{arrest!medics@of medics}.
This protection may snap away for just a single weekend and return the next if there is a large gathering of politicians or other drivers of global capitalism.
While in some cases it may protect you from the police, neutrality is even less effective against fascists, nationalists, and other right-wing groups.

\footnotetext[13]{
In November of 2019 during the siege of PolyU in Hong Kong\index{Hong Kong!2019--20 protests}, police\index{police!interference with medics} arrested\index{arrest!medics@of medics} 51 medics and journalists.\supercite{hong-kong-arrest-medics}
}

This book makes no claim of the moral superiority for either neutral or more obviously partisan medics, nor is there a claim that any class of tactics is more generally effective than the others for advancing a movement.\footnotemark[14]
The choice of how to operate depends on what a medic or collective is comfortable with or what they think is most effective for a given time and place.

\footnotetext[14]{
Personally, I operate as all three classes of medic depending on the action, region, and how rowdy I feel like getting.
One doesn't need to pigeonhole themself into any single class of medic.
}

Some medic collectives operate with a mix of medic classes, and what mix they have will depend on the nature of an action.
A tactic that you may find useful is to embed black medics with the Black Bloc\index{Black Bloc} while some more neutral medics operate in other parts of the action.
This allows partisan medics to immediately render aid to serious injuries and coordinate evacuation to treatment by the better equipped uniformed and street medics.

\section*{Solidarity}
\index{solidarity|(}

Each medic has their own personal ethics and politics, and a collective's politics are the sum of those of its members.
To these ends, if a medic or collective chooses to support actions or groups, this support may be seen as an endorsement of the action or group's politics.
In the struggle against capitalism, fascism, and the State, there are many factions.
You may find some to be strong allies and other simply tactical allies given the current climate.
There may be groups who you don't personally support even if they are working towards similar goals.

Some medics find that putting on a uniform removes some of their sense of individuality so that they may support actions they would not attend as themselves.
Medics may choose to support a group they don't fully agree with because, in the current context, they are more ally than enemy.
This may mean supporting a group who are targeted by neo-nazis even if that group has some problematic views.
It may mean providing assistance to an action organized by soft liberals knowing that they neglected to organize their own medical support.

While we, the broader left, should show a large degree of solidarity to those we don't like or completely agree with, our own personal opinions may get in the way.
We may additionally be burdened by rumors and accusations that come with whom we associate or appear to support.
The donning of a uniform can be used to brush these off to some degree.
Individuals and collectives can say ``Our presence at an action doesn't imply support, and we are here to provide medical support for many groups.''

In relation to dressing with neutrality, you may be granted additional privileges over other protesters.
This may be permission to leave kettles early or to cross police lines to get to public transit or into a car park.
Taking advantage of this may be necessary to do your duties, but you should avoid abusing this neutrality to serve yourself.
Other protesters may become resentful, and leaving actions when others must stay lacks solidarity and may foster resentment.
Whether or not this is an intentional police tactic to sew division is unclear.

\index{solidarity|)}

\section*{Collect Statistics}
\index{statistics|see {epidemiology}}
\index{epidemiology|(}

Medicine should aim to prevent injury and illness as much as it aims to treat it.
Epidemiology can help identify trends, and data can be used to influence policy.

The State, whether you trust it or not, can pass legislation that affect police behavior.
You can influence the State, and if you can't do it directly, you can influence media and public opinion.
Barring the abolition of the State and the police, what reform can be managed in the interim will prevent injuries, some of which are permanent, and loss of life.

Saying ``the police beat a lot of people last week'' is not a particularly convincing statement about police brutality.
Unless said acts are caught on camera, and unless they are reported to the police, such incidents might as well be made up for all the weight they carry.
Statistics and anonymous data gathered by medics can be compiled to show trends rather than just isolated incidents.

For an example of this, the Coordination Premier Secours\index{Coordination Premier Secours} (``First Aid Coordination'') in France\index{France} compiles data from medic collectives to create a census of the victims of police violence.\supercite{coordination-premier-secours-census}
This information is publicized in documents including anonymized data as well as summaries.
An example of their translated data from March 16th, 2019 can be found in \autoref{tab:patient_census_summary} and \autoref{tab:patient_census_summary}.

\begin{table}[htbp]
\caption{Patient Census Summary\supercite{coordination-premier-secours-march-16}}
\label{tab:patient_census_summary}
\centering
\begin{tabular}{|l|c|l|}
	\hline
    \multicolumn{1}{|c|}{\textbf{Weapon / Situation}} &
        \multicolumn{1}{|c|}{\textbf{Victims}} &
		\multicolumn{1}{c|}{\textbf{Of which\ldots}} \\
    \hline
    LBD40 / flashball       & 83     & 11 head injuries \\
    \hline
    GLI-F4 tear gas grenade & 20     & 2 head injuries  \\
    \hline
    Sting-ball grenade      & 50     & 12 head injuries \\
    \hline
    Tonfa / baton           & 37     & 17 head injuries \\
    \hline
    \ldots                  & \ldots & \ldots           \\
    \hline
    \multicolumn{1}{|c|}{\textbf{Total}} &
        \multicolumn{1}{|c|}{\textbf{394}} &
		\textbf{84 head injuries} \\
    \hline
\end{tabular}
\end{table}

\begin{table}[htbp]
\caption{Patient Census Data\supercite{coordination-premier-secours-march-16}}
\label{tab:patient_census}
\centering
% making this slightly under \textwidth to make it less ugly
\begin{tabularx}{0.9 \textwidth}{|l|X|}
	\hline
    \textbf{Victim 1} & Man, 60's. Bruise on the back, shot by LBD40. \\
    \hline
    \textbf{Victim 2} & Man, 20's. Cardiac distress, unknown cause. Evacuation by firefighters. \\
    \hline
    \textbf{Victim 3} & Man, 30's. Large gash on the head, ankle wound, shot by GLI-F4. Evacuation by civil protection. \\
    \hline
    \textbf{Victim 4} & Man, age not given. Hyperventilation, loss of consciousness, tear gassed. Evacuation by firefighters. \\
    \hline
    \textbf{Victim 5} & Woman, age not given, medic. Wounds on the back of the thighs from exploding GLI-F4. \\
    \hline
\end{tabularx}
\end{table}

If you choose to collect data on this, you should coordinate with other medics as well as legal observers\index{legal observers}.
Useful data points to collect are, in some semi-order of priority:

\begin{itemize}
    \item If the patient was a medic, journalist, legal observer, or other non-protester
    \item Nature of the injury or illness (location, description)
    \item Cause of the injury, especially noting whether or not it was caused by the police
    \item Treatment given
    \item Whether the patient required evacuation to advanced medical care
    % TODO are time / location too identifying?
    \item Exact location such as cross street or landmark
    \item Exact time
    \item Photo of injury, sufficiently anonymized
    \item Patient age, bucketed to decade (e.g., child, teen, 20's, 30's, etc.)
\end{itemize}

Gender is of questionable utility to collect.
Data you should not collect are name, exact age, or anything else that could be used to identify the patient.
Additionally, you may need to check with local laws\index{legality} about collecting patient data and how to sufficiently anonymize\index{anonymity!patients@of patients} it for reproduction.

\index{epidemiology|)}

\section*{Know Your Enemy}
\index{police!tactics|(}

In order to better understand police tactics, it may not be enough to actively observe them at demonstrations.
This may be helpful, but you should talk to other activists and medics in your region to share knowledge.
Read police manuals and browse their forums to see what they talk about and how they think.\footnotemark[15]
Watching videos online of police actions can also be useful, especially those of actions you have attended.
This gives you external data to compare what definitely happened to what you subjectively experienced.
Such footage can be truly enlightening.

\footnotetext[15]{
The 2016 film \textit{Do Not Resist} covers the 2014 Ferguson unrest and gives introductory insight to how the police act and think.
While it is true that the US is hyper-militarized, and it's also true that police tactics vary from region to region, there is still insight to be gained.
}

Preparation can help calm you by giving an idea of what to expect.
When police charge into a crowd, do they leave medics alone?
If so, you may be able to back against a wall and wait it out.
Do they beat and arrest everyone with zero regard as to what they were doing?
If so, you will know to expect no leniency.

\index{infiltrators|seealso {snitches}}
\index{infiltrators|seealso {informants}}
\index{informants|seealso {snitches}}
\index{informants|seealso {infiltrators}}
\index{snitches|seealso {informants}}
\index{snitches|seealso {infiltrators}}

\index{infiltrators|(}
As with all left-wing social movements, there will be attempts at infiltration from the State and fascists.\footnotemark[16]
There may additionally be snitches.\footnotemark[17]
You will have to develop some security culture, which is somewhat covered in \fullref{ch:opsec}, but identifying and outing snitches is out of scope of this book.
\index{infiltrators|)}

\footnotetext[16]{
\index{infiltrators|(}
Starting in 2004, ``Anna the medic'' worked as an infiltrator leading to the 2006 arrest of green anarchist Eric McDavid and two others.\supercite{anna-medic, anna-medic-take-part}
Anna used her position as a street medic as an in to groups, and when called upon utterly failed to provide aid leading to at least one hospitalization.
Anna is fucking scum.

She was not the first, and has not been the last.
In 2012, Seattle anarchists spotted another informant posing as a medic.\supercite{seattle-medic-informant}
\index{infiltrators|)}
}

\footnotetext[17]{
\index{informants|(}
An antifascist medic known as Tan was discovered to be a police informant in 2016 in Portland, Oregon.\supercite{medic-police-informant}
Tan was so shameless in their snitching that they wore a medic patch for the photo shoot for the article about their snitching.
They claimed to want to relocate to the Berkeley or Seattle protest scenes, completely misunderstanding the damage they caused or that they would not be trusted.
\index{informants|)}
}

Further, you will need to be aware the not all medic groups have a left or even centrist outlook.
While it is generally less common, right wing medic groups exist.
At counter-protest and liberal actions, they are of little concern.
During larger, more widespread unrest, they will try to pollute a movement, and they need to be driven out.
% TODO those German medics who showed up in Strasbourg or whatever
% TODO those German chud medics who show up to shit like nazi/RAC music festivals

% TODO ? \section*{Certifications}

\index{police!tactics|)}

\section*{Summary}

It may be possible for you and your collective to present as a neutral and unaffiliated group by carefully branding yourself and dressing like a medical professional.
This, plus a pleasant disposition when interacting with the police, may make it possible to act unhindered while at actions.
This is especially useful for crossing police lines, avoiding arrest, and being able to treat patients with out police intervention.
Acting and dressing neutral is no guarantee that law enforcement will respect you as a medic, but it may help.
There are some drawbacks to dressing and acting this way, and you may have political and ethical qualms with giving the appearance of respecting the State or showing anything but utter contempt for the police.
It is not strictly better to act neutral or not, and you and your collective will have to decide how you generally behave.

\index{tactics!general|)}
