\chapter{Medication}
\label{ch:medication}

\epigraph{
Give people what they need: food, medicine, clean air, pure water, trees and grass, pleasant homes to live in, some hours of work, more hours of leisure.
Don't ask who deserves it.
Every human being deserves it.
}{Howard Zinn, \textit{Marx in Soho}\supercite{marx-in-soho}}
\noindent

\index{medication|(}

There are different schools of thought on whether medics should carry or administer medication to patients, and these decisions are often based on both ethical and legal considerations.

The human body is complex, and medications have complex interactions with each other.
Without medical qualification to do so, administering medication can cause harm to the patient, even if it is a simple \acrlong{OTC} (\acrshort{OTC}) medication.

\index{legality!medication@and medication|(}
From a legal standpoint, you may be liable for any complications a patient suffers as a result of your administration of medication or facilitating a patient's self-administration as this calls out side your scope of practice\index{medics!scope of practice}.
In some regions, police may treat any pills, even if individually packaged and clearly labelled, as controlled substances and may use this as grounds to arrest\index{arrest!medics@of medics} you even if they know the charges will be later dropped\index{police!interference with medics}.
For these reasons, some medics will choose to not carry medication.
\index{legality!medication@and medication|)}

\section*{Administration}

Medics typically will make one of the three following choices on choosing to carry medication.
In this chapter, the term ``basic medication'' means medications like \acrshort{OTC} medication, oral glucose, salbutamol inhalers, and epinephrine autoinjectors.

\subsection*{Facilitating Self-Administration}

Carrying basic medication and facilitating patient self-administration is reasonably safe both in terms patient safety and legal risk.
If you choose to carry medication and think it is appropriate that the patient self-administers, you should be sure that the patient is of clear mind to do so.
Their level of consciousness should be A+O$\times$4 using the \acrshort{AVPU} scale, and they should be sober.
Additionally, it is important that you clearly state you medical qualifications and do not attempt to influence their decision to take the medication. For example, if your patient appears to be suffering an asthma attack following exposure to tear gas, you could say
``I am carrying a salbutamol inhaler. If you know how to use it and have taken this kind of medication before, you may want to consider doing so.
However, I am not a doctor, so I cannot say whether you should or whether it is safe to do so.''

\subsection*{No Medication}

Carrying no medication carries the lowest risk to both patient and medic, though patients may find it bothersome, ``unprofessional,'' or ``unprepared'' that medics do not have basic medication, in particular pain-relievers.
If you choose to not carry medication, be prepared to explain why you do not, namely that you are not medically qualified to administer medication, and that there is risk of harm to the patient and legal risk to you.

\subsection*{Administering Medication}

Carrying basic medication and administering it to patients is an acceptable practice, but facilitating self-administration is somewhat preferred.
Unless you have the appropriate training, there is risk of causing complications in a patient in an already chaotic environment, and you may face legal risk of administering medication while acting in a medical capacity.
If you choose to do so, you can take several steps to reduce the risk of harm.

Like with facilitating self-administration, the patient's level of consciousness should be A+O$\times$4\, and they should be sober.
Avoid administering medication that the patient has not taken before.
Naturally, both of these guidelines do not apply when administering naloxone or epinephrine as the patients may not be conscious.

Only carry a small number of common, well understood medications.
Memorize the \glspl{contraindication} for all medications you carry as well as potentially harmful drug interactions.
Consider printing this information on cards, laminating them, and putting them in your medication pouch so that you may quickly consult them before administering medication.\footnotemark[1]

\footnotetext[1]{
Additionally, there are a number of apps for mobile devices made for doctors and pharmacists.
These apps will include contraindications and dangerous drug interactions.
Some of these apps require a costly monthly or yearly subscription, and thus they may not be suitable for riot medics.
A review of these apps is outside the scope of this book.
}

\section*{Summary}

Administering medication can be harmful to the patient and may place you at legal risk.
You may want to consider neither carrying nor administering medication to protect patients and yourself.
If you choose to facilitate patients to self-administering, or if you choose to administer medications to patients, you should do so cautiously.

This book covers basic administration of medications to help educate medics.
Doing so is not an endorsement of carrying or administering medications, and it is done to help instruct medics on how to do so safely.
Likewise, the warnings in this chapter are not proscriptions against carrying medication.

\index{medication|)}
