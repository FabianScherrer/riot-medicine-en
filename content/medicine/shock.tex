\chapter{Shock}
\label{ch:shock}

\epigraph{But a more careful examination soon serves to show that deep mischief is lurking in the system; that the machinery of life has been rudely unhinged, and the whole system is shocked\ldots}
{Samuel D. Gross, MD\supercite[p. 410]{gross-surgery}}

\index{shock|(}

\noindent
Shock is a reduction in the ability to delivery oxygenated blood to the tissues of the body.
It is a secondary, and often lethal, component of many injuries including fractures, severe burns, and blood loss.
The onset of shock may be sudden or delayed and, and it may seem to be disproportionate in severity compared to the original injury.
Shock can lead to cascading metabolic and organ failure eventually sending the patient into an irreversible death spiral.
Proper and prompt management of shock can break this spiral, so it is critical that medics are able to identify patients both at risk for and in shock and begin interventions.

\section*{Overview}

Shock is a state of insufficient perfusion\index{shock} where the delivery of oxygenated blood is below tissues' demand leading to organ dysfunction.
Causes of shock can be simplified and grouped into the three main causes: decreased blood volume, decreased cardiac output, and decreased vascular tone.

\index{hypovolemia!shock@in shock|(}
\triplesubsection{Decreased blood volume}
Blood volume most obviously decreases when blood is hemorrhaged\index{hemorrages} either into the environment or into body cavities.
Any fluid loss can lead to decreased blood volume such as dehydration, diarrhea, or severe burns.
Loss of \SI{500}{\mL} of blood is enough to affect blood pressure, and loss of \SI{1500}{\mL} of blood can cause moderate shock.\supercite[pp. 37]{nols}
\index{hypovolemia!shock@in shock|)}

\index{cardiac output!shock@in shock|(}
\triplesubsection{Decreased cardiac output}
Cardiac output is the number liters of blood pumped by the heart per minute.
A heart attack\index{myocardial infarction} may damage heart tissue leading to reduced ability to pump.
Cardiac tamponade\index{cardiac tamponade} (fluid buildup in the sac around the heart) can compress the heart leading to decreased cardiac output.
\index{cardiac output!shock@in shock|)}

\index{vascular tone!shock@in shock|(}
\triplesubsection{Decreased vascular tone}
Vascular tone is the degree of constriction of blood vessels.
Decreased vascular tone (corresponding to increased dilation) increases the space that must be occupied by a fixed amount of blood.
Global vasodilation can occur due to release of histamine\index{histamine!shock@in shock} in an allergic reaction\index{allergic reactions!shock@in shock}.
A damaged or severed spinal cord may disrupt the autonomic pathways that control vascular tone leading to global vasodilation.
Widespread infection can lead to global inflammation.
% TODO differentiate sepsis / SIRS
\index{vascular tone!shock@in shock|)}

Decreased perfusion\index{perfusion} leads to insufficiency of oxygen and nutrients as well as a buildup of waste products.
Without sufficient oxygen, cells begin anaerobic respiration producing lactic acid leading to lactic acidosis\index{lactic acidosis!shock@in shock}.
The body is often able to compensate for these conditions, but the compensatory measures may fail leading to tissue and organ damage.
In order to protect the heart and brain from this damage, blood is directed from the rest of the body to these organs.

The classic presentation of shock is pale, cool, clammy skin and an increased heart rate.
These symptoms are the body's reaction to acute stress\index{stress response!shock@in shock} and are part of the ``fight or flight'' response.\supercite[p. 37]{nols}\index{nervous system!sympathetic!shock@in shock}
In a healthy person, these symptoms will subside, and their vitals will return to normal values.

While it may not seem so, shock can be deadly.
Shock can also be cryptic in that symptoms do not present early as a warning sign.
Shock is most easily treated in its early stages, so it is recommended that medics treat patient who could potentially have shock as if they do have shock until there evidence to the contrary.

\subsection*{Categories of Shock}

Shock is commonly divided into four major categories depending on the underlying causes: hypovolemic, cardiogenic, obstructive, and distributive shock.\supercite[p. 63]{tintinallis}
These categories manifest with similar symptoms, and in the context of riot medicine, treatment for all four is the same.
The different categories are included here to help you understand what other medical conditions may lead to shock to help you understand when to look out it.

\subsubsection*{Hypovolemic Shock}
\index{shock!hypovolemic|seealso {hypovolemia}}
\index{hypovolemia|seealso {shock, hypovolemic}}
\index{shock!hypovolemic|(}

\index{hypovolemia|(}
Hypovolemic shock is caused by decreased blood volume or decreased body fluids (hypovolemia).
Hypovolemia may itself be caused by a number of conditions such as blood loss from hemorrhage (both external and internal); loss of plasma from burns, oozing wounds, or lesions; or loss of sodium and water due to vomiting\index{vomit!hypovolemia@in hypovolemia} or diarrhea\index{diarrhea!hypovolemia@in hypovolemia}.
\index{hypovolemia|)}

Within riot medicine, hypovolemic shock will generally correspond to traumatic injuries with hemorrhaging or to broken bones accompanied by internal bleeding.
Estimating blood loss and assessing the criticality of blood loss are discussed in \fullref{ch:wound_management}.
Estimating loss of circulating blood due to bone fractures is discussed in \fullref{ch:fractures_and_dislocations}.

\index{shock!hypovolemic|)}

\subsubsection*{Cardiogenic and Obstructive Shock}
\index{shock!cardiogenic|(}
\index{shock!obstructive|(}

Cardiogenic and obstructive shock are caused by decreased cardiac output\index{cardiac output!shock@in shock}.
These two categories of shock present in much the same way and are often grouped together.

Cardiogenic shock is caused by the heart's inability to effectively pump.
The underlying cause may be a heart attack (myocardial infarction\index{myocardial infarction!shock@in shock}), blunt vascular injury, or a variety of heart diseases.
Obstructive shock is is caused by obstruction of blood flow into the heart via the major veins and arteries.
There are many causes, but medics will likely only be able to identify obstructive shock that arises from a tension pneumothorax\index{pneumothoraces!tension!shock@in shock} or pulmonary embolism\index{pulmonary embolism!shock@in shock}.

\index{shock!obstructive|)}
\index{shock!cardiogenic|)}

\subsubsection*{Distributive Shock}
\index{shock!distributive|(}

Distributive shock is caused by blood vessel dilation or other dysfunction to the blood vessels.
This leads to a drop in blood pressure (\gls{hypotension}) because the fixed volume of blood in the system must fill the increased volume of blood vessels.
Distributive shock may be subdivided into anaphylactic, neurogenic, and septic shock.

Anaphylactic shock is caused by anaphylaxis\index{anaphylaxis} (severe allergic reaction) leading to systemic vasodilation as a result of the body's release of histamine.
Neurogenic shock is caused by damage to the central nervous systems by either damage to the spinal cord or traumatic head injury.
This damage leads to decreased heart rate  and systemic vasodilation.
Septic shock is caused by systemic vasodilation an subsequent hypotension arising from infection.
It should be noted that medics likely will never encounter septic shock.

\index{shock!distributive|)}

\section*{Signs and Symptoms}

Shock can be difficult to identify because all signs and symptoms may not always be present in each patient, and there are different categories of shock with different manifestations of the symptoms.
The following signs and symptoms are generally present.

\triplesubsection{Cardiological observations}
The patient has a weak pulse.
Their systolic blood pressure may be under 90mmHg, however they may have normal systolic blood pressure and still be in shock.\supercite[pp. 64]{tintinallis}
They have an increased heart rate (over 100 \acrshort{BPM}) that may be irregular.

\triplesubsection{General feeling of unwellness}
The patient may be sweating and feel nauseous or be fatigued.
They may be anxious or restless or even irritable or aggressive.
The patient may have increased thirst.

\triplesubsection{Increased respiratory rate}
The patient may have an increased respiratory rate or be taking quick shallow breaths.
The patient may hyperventilate with quick, deep breaths.

\index{skin!shock@in shock|(}
\index{livedo reticularis|(}
\triplesubsection{Abnormal skin signs}
As blood is redirected from the skin, the skin may appear pale, cool, and clammy.
The skin may be mottled (livedo reticularis).
Mottled skin has red, blotchy, lace-like patterns (\autoref{fig:mottled_skin}).
\index{livedo reticularis|)}
\index{skin!shock@in shock|)}

\begin{figure}[htbp]
\centering
\caption{Mottled Skin\supercite{anon-1}}
\label{fig:mottled_skin}
\includesvg[width=8cm, keepaspectratio]{mottled-skin}
\end{figure}

\index{skin!turgor|(}
\triplesubsection{Signs of hypovolemia}
Patient in hypovolemic shock may have low jugular venous distension, muscle cramps, and dry mucous membranes.
They may have decreased skin turgor where their skin does return to its original shape when pinched.
% TODO skin turgor image / better explanation
\index{skin!turgor|)}

\index{thermoregulation|(}
\triplesubsection{Poor thermoregulation}
The patient may not be able to regulate their body temperature.
They may be uncharacteristically warm or cool.
\index{thermoregulation|)}

\triplesubsection{Late signs of shock}
As shock progresses, the patient's heart rate will increase, and their blood pressure will decrease.
As the brain becomes hypoxic, the patient's pupils may dilate or become slow to react to change in light.
This will additionally lead to changes in level of consciousness or alertness as well as confusion.
Late stage hypovolemic shock may also include decreased urine output, but the time scale for this symptom to become apparent is longer than most actions.

% TODO pulse oximetry
% TODO cyanosis

\subsection*{Symptom Variations}

Different categories of shock have the following signs and symptoms.
These variations may be in addition to or they may contrast with the shared symptoms.

\index{shock!neurogenic|(}
\triplesubsection{Neurogenic shock}
Due to nerve damage, the patient may be in respiratory arrest\index{respiratory arrest!neurogenic shock@in neurogenic shock}, and they may have massive and instantaneous systemic vasodilation leading to warm and flushed skin.
This vasodilation may lead to priapism\index{priapism} (an erect penis in the absence of stimulation).
Patients in neurogenic shock may have a decreased heart rate.
\index{shock!neurogenic|)}

\index{shock!cardiogenic|(}
\triplesubsection{Cardiogenic shock}
The patient may have distended jugular veins (\autoref{fig:distended_jugular_veins}).
\index{shock!cardiogenic|)}

\begin{figure}[htbp]
\centering
\caption{Distended Jugular Veins\supercite{anon-1}}
\label{fig:distended_jugular_veins}
\includesvg[width=6cm, keepaspectratio]{distended-jugular-veins}
\end{figure}

\index{shock!septic|(}
\triplesubsection{Septic shock}
The patient may have a fever instead of being cool.
\index{shock!septic|)}

\section*{Treatment}

The release of stress hormones from physical exertion, fear, or the stresses of physical engagement with the State will produce symptoms that are similar to those of shock, though these symptoms will quickly subside in healthy adults.
Conversely, patients with minor injuries (or even no injury at all) may still develop shock.
Shock is more likely in patients with multiple or severe injuries.
However, you should consider treating all patients with the anything beyond minor injuries for shock, or at the very least request that they sit with you for monitoring for at least 10 minutes.

Due to the potentially life-threatening nature of shock, the ideal treatment is by advanced medical care.
If this is not possible, field treatment may be possible.
Treatment for shock is a slow process, and the patient will need both time and calmness to recover.
For these reasons, shock cannot be treated in a conflict zone, and it is generally inadvisable to treat a patient for shock in the middle of a crowd.
If possible, you should quickly transport the patient to a safe place before beginning treatment.

\triplesubsection{Call EMS}
If \acrlong{EMS} are available in your region, call EMS using the steps covered in \fullref{ch:patient_assessment}.

\triplesubsection{Treat critical injuries}
As with all patients, the first priority is to maintain the ABCs.
Before moving on to treating shock itself, the patient's primary injuries need to be treated or at least stabilized.
This includes controlling hemorrhage or treating contamination of their face by riot control agents.

\triplesubsection{Position patient for recovery}
If the patient is alert, position the patient in the \gls{supine} position.
If they are unconscious or disoriented and nauseated, position the patient in the recovery position.

\index{normothermia!shock@in shock|(}
\triplesubsection{Help maintain normothermia}
The patient may not be able to adequately regulate their body temperature, and depending on the weather, you may need to help warm or cool the patient.
An unclothed, normothermic patient will neither gain nor lose heat at \SI{28}{\celsius},\supercite{accidental-hypothermia-wilderness} so most likely treatment will be to help the patient stay warm.

In cool weather, place a mat, tarp, jackets, or (as a last resort) an emergency blanket under to patient to insulate them from the cool ground.
Wrap the patient in an emergency blanket or jackets to help keep them warm (\autoref{fig:shock_treatment}).
Additional information on treating and preventing hypothermia can be found in \fullref{ch:cold_injuries}.

On warm days, move the patient into the shade and consider removing extra clothing.
Use a tarp or emergency blanket held a distance greater than \SI{1}{\meter} from the patient to provide shade.
Consider using cool water on bandanas or and fanning to help cool the patient.
Additional information on treating hyperthermia can be found in \fullref{ch:heat_illnesses}.

\begin{figure}[htbp]
\centering
\caption{Treatment for Shock\supercite{anon-1}}
\label{fig:shock_treatment}
\includesvg[width=10cm, keepaspectratio]{shock-treatment}
\end{figure}

\index{normothermia!shock@in shock|)}

\triplesubsection{Consider administering fluids}
Patients who may imminently require surgery or intensive care should not be given fluids.
In urban environments where the patient can be rapidly evacuated to a hospital, fluids should not be administered.
In wilderness environments where evacuation may be delayed by hours, patients who are alert and can tolerate drinking may be encouraged to drink a small amount of cool to lukewarm water.
This should be limited to 1 or 2 mouthfuls at a time, separated by 10 to 15 minutes reduce the risk of aspiration should the patient vomit.
Water may be mixed with a small amount of salt, bullion, or flavoring, though the salt content should not exceed that of saline (0.90\% \acrshort{w/v}) which corresponds to approximately one small spoonful of salt per 1 liter of water.
Drinks should not be excessively sweet or salty and should not contain strong flavors.

\triplesubsection{Monitor ABCs}
The patient's vitals should be continuously monitored until they can be evacuated to advanced medical care.

\section*{Urban Legends}
\index{urban legends!shock@in shock|(}
\index{Trendelenburg position|(}

To treat shock, some first aid courses teach that the patient should be laid back in an inverse recline position (Trendelenburg position) or flat with their their legs elevated (passive leg raise / modified Trendelenburg position) in order to help move blood from the legs to the torso and head.
This does not appear to produce clinically significant physiological changes, may actually be harmful to patients, and may cause discomfort in patients when left in this position for extended periods.\supercite{trendelenburg-myth, trendelenburg-blood-volume-distribution, trendelenburg-leg-position, tintinallis}
Complications include increased intracranial pressure, difficulty breathing due to gravity pressing organs against the diaphragm, and aspiration of vomit or other fluids.
For these reasons, the patient should simply be laid back on a flat surface in the supine position.\footnotemark[1]

\footnotetext[1]{
This recommendation is not a hard rule, and in some patients the use of the Trendelenburg position may improve cardiac output.\supercite[pp. 67]{tintinallis}
However, medics have no way of measuring this, and there are enough cases where the Trendelenburg position can cause harm.
For these reason, the simplified rule is to always use the supine position.
}

\index{Trendelenburg position|)}
\index{urban legends!shock@in shock|)}

\section*{Summary}

Shock is a complex and life-threatening medical condition consisting of inadequate perfusion of the body's tissues that often presents alongside other injuries, though it may appear in patients with no injuries at all.
Symptoms of shock are often pale, cool, clammy skin; sweating; irritability, nervousness, or aggression; elevated heart rate; and decreased blood pressure.
Minor shock may self-correct, but if left untreated, shock may progress to irreversible tissue damage followed by death.
Shock is treated by monitoring the patient's ABCs and maintaining their body temperature.
When in doubt, treat patients for shock and evacuate them to advanced medical care.

\index{shock|)}
