\chapter{Alternative Medicine}

\index{alternative medicine|seealso {urban legends}}
\index{urban legends|seealso {alternative medicine}}
\index{pseudoscience|see {alternative medicine}}
\index{pseudoscience|see {urban legends}}
\index{medicine!traditional|see {traditional medicine}}
\index{medicine!folk|see {folk medicine}}

\index{alternative medicine|(}

\epigraph{To be truly visionary we have to root our imagination in our concrete reality while simultaneously imagining possibilities beyond that reality.}
{bell hooks, \textit{Feminism is for Everybody}\supercite[p. 110]{feminism-is-for-everybody}}

\noindent
Riot medics' knowledge is often passed along from one medic to another, and even in cases where genuine benefit is gained from certain treatments or practices, there are not clinical trials or published research to back claims.
Thus, along with the valid knowledge, urban legends are circulated and taken to be true.
There is evidence supporting use of traditional\index{traditional medicine} and folk medical\index{folk medicine} treatments for illnesses and injuries, and this chapter is not attempting to claim that all traditional and folk medicine is quackery.
However, if you are considering using any treatments, they should be backed by credible research or at the very least have a strong basis in biology, chemistry, and physics.

Furthermore, one must also consider the efficacy of folk medicine compared to what is used in hospitals and by doctors.
While some folk medicine remedies and practices may be effective, they are often neither more effective nor cheaper than what one finds in hospitals and textbooks.
An in depth discussion of this is outside the scope of this book.

This chapter was added because in the course of researching street medics' practices for this book, it was found that there were blogs, wikis, zines\index{zines} going back over a decade, and other street medic resources that recommended alternative medical practices and treatments that have been widely debunked and considered shams.
This chapter attempts to address these urban legends.
Other urban legends are addressed in their relevant chapters.

\section*{The Left and Medicine}

\index{anarchism!medicine@and medicine|(}

Beliefs in alternative medicine, as well as more general antiscientific beliefs, exist throughout the modern world.
They can be found in many countries and nearly all political ideologies.
Anarchist spaces are not immune to beliefs in alternative medicine or outright antiscience.

The same skepticism anarchists use to question the so-called ``natural order'' of the world around them is also turned towards the institution of Science Itself.
In living memory, there have been obvious failures of science.
Thalidomide caused birth defects, there have been numerous nuclear meltdowns, and the ``science'' of economics has repeatedly brought the world economy crashing down.
Elements within the hippie movement of the 1960's equated science with the establishment and ``the man.''
The anti-GMO, anti-nuclear, and anti-Big-Pharma movements are more recent currents within the left that have led to skepticism against institutional science.
In particular, with regards to medicine, the 1990's saw wide popularity of medical advice often being ``just take a pill'' before being replaced with approaches that were holistic and more ``natural,'' though still backed by science.

Rightfully, science should be questioned.
Scientists may chase evidence that proves their own biases, and \glspl{pseudoscience} like phrenology experienced wide popularity before being generally discredited.
Science is also a fundamentally hierarchical discipline that often inadvertently, or even intentionally, protects the status quo.
The anarchist Ursula K. Le Guin wrote about this in her 1974 novel \textit{The Dispossessed}.
In his autobiography, Max Planck described the conservative nature of science which went on to be known as the Planck Principle.\supercite{max-planck-autobiography}

\begin{displayquote}
An important scientific innovation rarely makes its way by gradually winning over and converting its opponents: it rarely happens that Saul becomes Paul.
What does happen is that its opponents gradually die out, and that the growing generation is familiarized with the ideas from the beginning: another instance of the fact that the future lies with the youth.
\end{displayquote}

The Planck Principle itself was itself studied and shown that following the unexpected death of superstars within a field, authors who had not previously collaborated with the deceased had an increase in publications.\supercite{superstar-deaths}

This top-down approach and resistance to change can be true in anarchists spaces, but the opposite can also be true.
Esoteric knowledge, like street medicine, is passed on by word of mouth and in zines from the older anarchists to the younger.
In some regions, many anarchists ``age out'' of the movement in their early thirties, and institutional knowledge is quickly lost.
Anarchists are also more inclined to trust other anarchists than the types of institutions they fight against.
The use of folk science\index{folk science} and ``works for me'' as criteria for continued usage of a particular treatment leads to antiscientific practices.\footnotemark[1]
While this is an effective way of sharing and conveying information in a decentralized and non-hierarchical way, it results in misinformation being taken as gospel and further distorted through some sort of game of telephone.

\footnotetext[1]{
\index{operational security|(}
Another place where anarchists are antiscientific is around counter-surveillance and anti-repression\index{anti-repression} where tactics that worked decades ago may still be widely deployed despite changing police tactics, new technology, and actual ineffectiveness of that tactic.
\index{operational security|)}
}

\index{anarchism!medicine@and medicine|)}

\section*{Debunked Treatments}

Not all pseudoscientific medicine can be individually addressed, and there is insufficient time to argue against each or in favor of evidence based medicine.
Aside from the ones listed here, other alternative medical treatments have been claimed to have been used by medics, and all should be avoided.
This includes, but is not limited to, magnet therapy\index{magnet therapy!urban legends@in urban legends}, reiki\index{reiki!urban legends@in urban legends} (``energy'' transfer), crystal healing\index{crystal healing!urban legends@in urban legends}, and cupping therapy\index{cupping therapy!urban legends@in urban legends} (the use of suction cups placed on the skin).

\subsection*{Homeopathy}

\index{homeopathy|(}

Homeopathy is, in part, the practice of repeatedly diluting a chemical until virtually none of the original chemical remains (and in some cases not a single molecule remains).
Proponents of homeopathy claim that via ``water memory'' the water retains the chemical and pharmacological properties of the original chemical that was present prior to dilution.\supercite{smith-2012}
This claim goes against modern understanding of biology, chemistry, and physics, or as one author put it: ``the main assumptions of homeopathy are biologically implausible.''\supercite{homeopathy-best-evidence}

Medics have written about using homeopathic remedies to treat different illnesses and injuries including riot control agent contamination\index{riot control agents!urban legends@in urban legends} and blunt trauma\index{trauma!treatment@treatment of!urban legends@in urban legends}\index{urban legends!trauma@in trauma}.
Medics must never use homeopathic treatments or recommend patients seek further treatment via homeopathic remedies.
At best, homeopathy performs no better than placebo,\supercite{homeopathy-clinically-valuable, homeopathy-best-evidence, homeopathy-placebo-trials} and studies that claim otherwise tend to have poor design.\supercite{homeopathy-efficacy, homeopathy-review-of-reviews}
The following is stated in no uncertain terms: homeopathy is not medicine.

\index{homeopathy|)}

\subsection*{Paraherbalism}
\index{paraherbalism|(}

The use of plants as medicine extends far back into human history and can even be found in non-human animals.\footnotemark[2]
Herbalism\index{herbalism} is the study of botany and medicinal plants using the techniques of evidence based medicine.
Paraherbalism is the pseudoscientific use of parts of plant and animals to create medicine and other health-promoting agents.
Paraherbalism tends to be linked to naturopathy\index{naturopathy}, or the belief that the body can always self-heal and that natural and unprocessed products can guide the body's ``vital energy.''
In general, medics should not use herbs or other plants to treat patients because of their limited efficacy and possible negative interactions.

\footnotetext[2]{
Animals using naturally occurring substances such as a plants and soils to self-medicate is known as zoopharmacognosy.
Such behavior is fascinating.
}

\subsubsection*{St. John's Wort}
\index{Hypericum perforatum|see {St. John's Wort}}
\index{St. John's Wort|(}

A number of medic resources recommend St. John's Wort (Hypericum perforatum) as treatment for illness and injuries including riot control agent\index{riot control agents!urban legends@in urban legends} contamination, bruising, nerve damage, and acute stress.
It is suggested to be administered as a tea, using extracted oils, or in pill form.

\begin{figure}[htbp]
\centering
\caption{St. John's Wort}
% public domain: https://commons.wikimedia.org/wiki/File:61_Hypericum_perforatum_L.jpg
\includesvg[height=7cm, keepaspectratio]{st-johns-wort}
\end{figure}

St. John's Wort alone can cause adverse drug reactions such as gastrointestinal irritation\index{gastrointestinal tract!St. John's Wort@in St. John's Wort}, nausea, headache, allergic reaction\index{allergic reactions!St. John's Wort@in St. John's Wort}, fatigue, and restlessness.\supercite{st-johns-wort-review}
It may cause photosensitivity\index{photosensitivity!St. John's Wort@in St. John's Wort} leading to sunburns in conditions that would not harm most humans.\supercite{st-johns-wort-review}
It may also interact with other foods or drugs leading to dangerously high blood pressure and serotonin syndrome\index{serotonin syndrome!St. John's Wort@in St. John's Wort} (when taken with serotonergic medications like \acrshort{SSRI}s\index{selective serotonin reuptake inhibitors!St. John's Wort@in St. John's Wort}).\supercite{st-johns-wort-adverse-effects}
St. John's Wort also increases the metabolism of certain estrogens\index{estrogens!St. John's Wort@in St. John's Wort} which decreases their levels in the body.
In particular, this can reduce the efficacy of hormonal contraceptive medication\index{contraceptive medications!St. John's Wort@in St. John's Wort}.

Use of St. John's Wort can be summarized with the following quote:\supercite{st-johns-wort-review}

\begin{displayquote}
Given what is currently known and unknown about the biological properties of [St. John's Wort], those who choose to use this herb should be closely monitored by a physician.
\end{displayquote}

Medics are (typically) not physicians. They should not administer St. John's Wort.

\index{St. John's Wort|)}
\index{paraherbalism|)}

\subsection*{Acupuncture}
\index{acupuncture|(}

Acupuncture is the practice of inserting thin needles into the body, often as a means of pain relief.
There have been cases of street medics recommending acupuncture as a treatment for acute stress and pain.
Acupuncture is considered pseudoscience and has limited to no efficacy.
Medics should not treat patients with acupuncture.
Under ideal conditions with sterile needles, there is non-negligible risk of infection as well as traumatic pneumothorax\index{pneumothoraces!traumatic!acupuncture@and acupuncture}.\supercite{acupuncture-review}
In riot conditions, it is unlikely that a medic will be able to achieve acceptable sterility of their equipment to safely administer acupuncture as a treatment, not to mention the risk injury as a result of leaving needles in a patient during a physical confrontation.

\index{acupuncture|)}

% TODO chiropractic / osteopathy

\section*{Summary}

Using alternative medicine to treat your patients or encouraging them to use such treatments can cause harm in as much as they replace genuine therapies.
For this reason they must be avoided.
Your patients will learn from you and try to emulate the treatments you have administered.
Keeping treatments simple and using evidence based medicine will help steer them towards better self-treatment in the future.

\index{alternative medicine|)}
