\chapter{Heat Illnesses}
\label{ch:heat_illnesses}

\epigraph{Global heating will severely challenge civilisation in some areas and probably vanquish it in others.}
         {Anonymous, \textit{Desert}\supercite{desert}\textsuperscript{,}\footnotemark[1]}

\footnotetext[1]{For a look a what the future holds for both the environment and anarchism, read \textit{Desert}.}

\index{heat illnesses|(}

\noindent
Heat illness is a range of illnesses caused by exposure to hot environments.
These may be quite minor in nature (nothing more than a nuisance), or they may be fatal if not rapidly treated.
Proper clothing and hydration are significant mitigations, but the Black Bloc doesn't rest during the summer.
Weather that may be only unpleasantly warm for well prepared protesters may be dangerous for black clad anarchists running through the streets.

A direct result of anthropogenic climate change is that there will be hotter, more humid weather globally.\supercite[p. 956]{climate-change}
Historically, heat waves have lead to spikes in heat related deaths, and these deaths disproportionately affect the poor, elderly, and minorities,\supercite{france-heat-wave, chicago-heat-wave} not to mention the houseless.\supercite{climate-change-homeless-deaths}
Ignoring other causes like fires and asthma, climate change will lead to increasing deaths as a result of extreme heat.\supercite{climate-change-extreme-heat}

Though many of these deaths during heat waves do not occur in settings where medics are present, an increasing number of hot and humid days means that more actions where take in weather that can heat stress demonstrators.
This may be especially true during remote actions and occupations where participants cannot easily escape to a climate controlled building.

The sections on physiology and thermoregulation in \fullref{ch:cold_injuries} are considered prerequisites for this chapter.

\section*{Physiology}

% TODO it would be nice to add a discussion of solute, osmolarity, ingestion, kidney removal of waste, and all that jazz in order to really drive home some of the importance of proper diet and hydration, but honestly i just don't feel like it at the moment. maybe this is something for the second edition. and if this were done, this section could be better split into: hypothalamus stuff, water loss stuff, and nutrition stuff.

The hypothalamus\index{hypothalamus!thermoregulation@in thermoregulation} controls thermoregulation by inducing cooling or heating mechanisms to get our core temperature to reach a given set-point.
Typically this hypothalamic set-point is 36.5 to \SI{37.5}{\celsius} (normothermia)\index{normothermia}.
When we get sick, the set-point is elevated to help us fight pathogens.
This difference in temperature between our current core temperature and our set-point is why we shiver when we have fevers.
\Gls{hyperthermia}\index{hyperthermia} is a core temperate of \SI{37.5}{\celsius} or higher caused by external heating.

\index{pyrexia|see {fever}}
\index{fever|(}
A fever, also known as pyrexia, is distinct from \gls{hyperthermia}.
A fever is the body intentionally increasing core temperature, whereas hyperthermia is an increased temperature due to an inability to cool.
That said, fevers over \SI{40}{\celsius} are classified as hyperpyrexia\index{hyperpyrexia} and are a medical emergency.
\index{fever|)}

\index{dehydration|seealso {water loss}}
\index{water loss|(}
\index{sweating|(}

The human body loses water through both sensible and insensible ways.
Sensible water loss includes urination, sweating, vomiting, and diarrhea.
Water loss from sweat during exercise in hot weather can have a sustained rate of \SI{500}{\mL} with peak rates of \SI{1200}{\mL} per hour.
Insensible water loss which primarily occurs through the respiratory tract and skin.
The skin is a poor water barrier.\supercite[pp. 44]{biology-human-survival}
In normal conditions, the body loses approximately \SI{400}{\mL} per day through the skin, and in hot and dry conditions, this loss can be over \SI{1000}{\mL}.\supercite[pp. 44]{biology-human-survival}
Normal water loss from the respiratory tract is \SI{200}{\mL} per day, but while marching in dry conditions, this can be as high as \SI{450}{\mL} per day.\supercite[pp. 44]{biology-human-survival}

\index{heat adaptation|(}
The body can be acclimatized to heat allowing for more exercise at higher temperatures without suffering heat injury.
Acclimatization leads to sweating at lower temperatures, dramatically increased sweat production (up to \SI{3}{\liter/\hour}), and longer sustained periods of sweating.\supercite[p. 1366]{tintinallis}
The heart rate is lower for a given temperature, and there is a general increased tolerance for exercising in heat.\supercite[p. 1366]{tintinallis}
Generally, acclimatization can be achieved in 10 to 14 days with 1 to 2 hours of exercise in heat, and this adaptation persists for approximately 1 month.\supercite{heat-injuries-wilderness-2019}
\index{heat adaptation|)}

\index{sweating|)}

Elderly\index{geriatrics!heat illnesses@in heat illnesses} individuals have dulled ability to sense heat and thirst.
Their thermoregulation responses to heat (sweating and vasodilation) begin at higher temperatures, and they sweat less.
For these reasons, they are at higher risk for heat illness than younger individuals.
Similarly, children\index{pediatrics!heat illnesses@in heat illnesses} sweat less than adults and may be unable to communicate their illness to their caretakers.
It may be assumed that they are just tired, irritable, or grouchy when in reality they have minor heat illness or dehydration.

\index{water loss|)}

\subsection*{Tonicity}
\index{tonicity|(}

The tonicity of a solution is a measurement of the potential of a solution to cause the flow of water across a semipermeable membrane.
Tonicity is determined by the amount of solute dissolved in a solution.
In human biology, the tonicity of a solution is whether or not it cause water to move in or out of cells.
For example, isotonic saline is 0.90\% \acrshort{NaCl} \gls{w/v}.
Solutions that have a higher concentration of solute of hypertonic.
Solutions that have a lower concentration of solute are hypotonic.

\index{tonicity|)}

\section*{Thermoregulation}
\index{thermoregulation|(}

If the ambient temperature is above the temperature of our skin, we gain heat from the surrounding atmosphere.
We also gain radiated heat from the sun and other warm objects such as buildings or asphalt.
% TODO heat temperature increase of sun vs. shade

Evaporation is the primary means by which we cool ourselves.
As sweat evaporates, heat is lost into the environment.
The rate at which evaporation happens depends on the relative humidity of the surrounding atmosphere.
At a relative humidity of 100\%, no evaporation occurs.
When the weather is hot, we feel hotter on more humid days, and when the weather is cold we feel colder on drier days.

% TODO image of WBGT thermometer (just for funsies)

The \acrlong{WBGT} (\acrshort{WBGT}) attempts to combine the effects of temperature, humidity, wind, and radiated heat to give a single, modified temperature reading.
This is done by but placing a dark bulb thermometer in the sun and a thermometer with a wet cloth over it in the shade and allowing wind to pass over it.\supercite{wbgt}
The temperature calculated by this method is a ``worst case'' as it assumes a person is wearing dark clothing and isn't being cooled by additional fanning.
Even the most fit and healthy humans cannot survive a \acrshort{WBGT} of \SI{35}{\celsius} for any sustained amount of time.\supercite{climate-change-heat-stress}

Children, the elderly, the ill, and fatter people will be impacted at lower \acrshort{WBGT}.
Exercising in the heat increases the risk of heat injury as the body is additionally heated internal by its metabolic processes.
People who have not been acclimatized to heat will are at greater risk of heat injury especially while exercising.

% TODO reference WBGT chart here

\index{thermoregulation|)}

\section*{Prevention}

Prevention of heat injuries require preparation and education.
Much of this work needs to be done before an action.

\triplesubsection{Coordinate with the community}
These prevention efforts require coordination with the community in which you exist.
Work with the organizers of an action to help spread tips on how to prevent heat illness in the days leading up to an action.
Zines\index{zines} and educational images posted to social media\index{social media} can be very helpful.
If the predicted temperature and humidity are expected to be high, the organizer may need to issue a warning to high risk groups or even cancel the event all together. % TODO how high temp/humid?
Actions in hot weather should have aid stations with shade and water.
This is more important for larger actions that draw more elderly participants.
During radical actions, such aid stations are unlikely to be useful or even possible.

\index{clothing!heat illnesses@in heat illnesses|(}
\triplesubsection{Dress appropriately}
Clothing should be loose fitting, light in color, and breathable to allow sweat to evaporate.
Hats, especially those with large brims, can keep direct sunlight off the head, face, and neck.

Dressing appropriately in hot weather has to be weighed against the need for safety and anonymity\index{anonymity}.
Individuals may wear pants, long sleeve shirts, hats, or balaclavas to obscure their identity.
\index{Black Bloc!heat illnesses@in heat illnesses|(}
Members of the Black Bloc may be totally totally covered in dark clothing making them more susceptible to heat illness than other participants.
However, this is countered by the general youth and vitality of Black Bloc members, the increased risk cause by the their clothing may be somewhat negated.
\index{Black Bloc!heat illnesses@in heat illnesses|)}

\index{clothing!heat illnesses@in heat illnesses|)}

\triplesubsection{Bring an umbrella}
An umbrella can provide portable shade during an action.
They are also useful for obscuring parts of the crowd from video surveillance.\footnotemark[2]

\footnotetext[2]{
\index{Hong Kong!2019--20 protests}
The protesters in Hong Kong during the unrest of 2019--20 have found great success in using umbrellas to create a modern testudo formation.
}

\triplesubsection{Bring water}
Based on the described rates of sensible and insensible water loss, it should be clear that a large amount of water must be consumed during moderate exercise in hot weather.
However, and stated anecdotally, individuals may intentionally underhydrate to avoid the need to urinate during actions.
This has some practical value as actions may suddenly kick off, and no one wants to run or fight with a full bladder.
Additionally, there may be few places to urinate during actions (especially for participants without penises), and public urination may carry significant legal penalties.

It is still worth attempting to counter intentional underhydration.
Medics should encourage individuals to drink sufficient water.
Protesters (and medics) should bring at least \SI{1}{\liter} of water for themselves, or better yet, athletic drinks with electrolytes.
Medics are massively outnumbered by non-medics, and their bags are typically already full of medical gear, so they likely cannot carry enough water bottles to hand out.
Medics may help run aid stations where this is feasible, or they may coordinate with other helpers to carry water to pass out to participants.

\triplesubsection{Acclimatize}
Participants should spent 1 to 2 hours outside in the weeks leading up to a hot action.
Even without exercising it heat, individuals will have some acclimatization.

Medics may see increased numbers of heat injuries during warmer weather early in the spring and summer as well as the first hot days of summer due to a lack of acclimatization.
This is somewhat anecdotal, but it is supported by evidence from mass participation running events.\supercite{heat-injuries-canadian-runs, heat-injuries-peach-tree}
Mass mobilizations that draw people from different regions may also have increased risk as they may be neither adequately prepared for the weather nor acclimatized.

\index{alcohol!heat illnesses@in heat illnesses|(}
\triplesubsection{Avoid alcohol}
Alcohol is a diuretic and will lead to increased water loss.
It is also a \acrshort{CNS} depressant and will lead to impaired thermoregulation.
Anecdotally, some participants feel that as long as they are drinking \textit{something} they are hydrating, so they will not drink the required additional non-alcoholic fluids.
\index{alcohol!heat illnesses@in heat illnesses|)}

\triplesubsection{Know which medications and drugs increase risk}
Some medications and recreational drugs increase the risk of heat illness.
Taking medications such as
antihistamines\index{antihistamines!heat illness@in heat illness},
beta blockers\index{beta blockers!heat illness@in heat illness},
calcium channel blockers\index{calcium channel blockers!heat illness@in heat illness},
and \glspl{anticholinergic}\index{anticholinergics!heat illness@in heat illness} is a risk factor.\supercite{heat-injuries-wilderness-2019}
Use of recreation drugs such as MDMA (ecstasy)\index{MDMA!heat illness@in heat illness},
cocaine\index{cocaine!heat illness@in heat illness},
and amphetamines\index{amphetamines!heat illness@in heat illness} also is a risk factor.
Medics will not be able to memorize a complete list of all drugs and medication that put individuals at risk of heat illness.
They best they can do is remind the community to check their medication's side effects for warnings about increased body temperature or decreased sweating.

\index{medics!self-preservation|(}
\triplesubsection{Protect yourself}
Medics may have more gear than other participants.
A large backpack means less surface area for evaporative cooling.
The added weight means additional exertion relative to other participants.
Carrying and wearing personal protective equipment may also put medics at risk for heat illness.
Medics can only help injured protesters if they are uninjured themselves.
Remember to stay hydrated and stay cool.
\index{medics!self-preservation|)}

\section*{Dehydration}
\index{dehydration|seealso {hypovolemia}}
\index{hypovolemia|seealso {dehydration}}
\index{hypovolemia|(}
\index{dehydration|(}

Dehydration is a state of deficiency of the total amount of water in the body.
Dehydration may be caused by sweating, vomiting, diarrhea, or increased urination as result of illness or taking diuretics.
Causes of dehydration at actions tend to be prolonged exposure to heat, insufficient hydration, and the consumption of alcohol.

\subsection*{Signs and Symptoms}

Classic signs and symptoms of mild to moderate dehydration are thirst and a dry or sticky mouth, however a person may still be dehydrated without experiencing thirst.
Other signs are dark yellow urine, headache, muscle cramps, and skin that is dry and cool.

Signs and symptoms of severe dehydration are decreased or absent urine output, very dark urine, dizziness, tachycardia, tachypnea, sunken eyes, lethargy, and syncope.
Patients with severe dehydration may go into hypovolemic shock\index{hypovolemic shock!dehydration@in dehydration}.
As electrolytes in the body become imbalanced, patients may have seizures.

\subsection*{Treatment}
\index{rehydration|(}

Treatment for dehydration is rehydration.

If the patient can drink on their own, give them, water, sports drinks with electrolytes, or oral rehydration solution.
Soda and full-strength juice are not recommended in patients with diarrhea.
Do not rehydrate patients using drinks containing alcohol.
Patients should not be given solid foods until they are rehydrated as this can worsen dehydration.

\index{oral rehydration therapy|(}
The ideal method of rehydration is oral rehydration therapy which uses a mixture of \acrlong{ORS} (\acrshort{ORS}).
The \acrlong{WHO} (\acrshort{WHO}) formula is found \autoref{tab:oral_rehydration_solution}.
A simplified recipe uses \SI{1}{\liter} of water, \SI{25}{\gram} of sugar (approximately one handful), and \SI{3}{\gram} of salt (approximately one pinch).
\index{oral rehydration therapy|)}

\begin{table}[htbp]
\caption{\acrshort{WHO} Formula for \acrshort{ORS}\supercite{who-ors}}
\label{tab:oral_rehydration_solution}
\centering
\begin{tabular}{l|l}
    \multicolumn{1}{c|}{\textbf{Ingredient}} &
		\multicolumn{1}{c}{\textbf{Amount}} \\
	\hline
    Water                        & \SI{1}{\liter}   \\
    Sodium chloride              & \SI{2.6}{\gram}  \\
    Glucose, anhydrous           & \SI{13.5}{\gram} \\
    Potassium chloride           & \SI{1.5}{\gram}  \\
    Trisodium citrate, dihydrate & \SI{2.9}{\gram}  \\
\end{tabular}
\end{table}

\index{rehydration|)}

\subsection*{Urban Legends}
\index{urban legends!dehydration@in dehydration|(}
\index{dehydration!urban legends@in urban legends|(}

Medics generally operate in environments with readily available potable water.
However, during wilderness actions, there may be situations where water is not available, and individuals may attempt to resort to survivalist urban legends.\footnotemark[3]
These include drinking seawater and drinking one's own urine.

\footnotetext[3]{
Yes, I fully understand the irony of describing ``wilderness'' ``urban'' legends.
}

\triplesubsection{Drinking seawater}
As with many things in the are of medicine, the answer is complex.

\begin{displayquote}
How much seawater can one ingest before it becomes lethal?
The practical answer to this question is ``None'' but the scientifically correct answer is ``It depends.''\supercite[pp. 55]{biology-human-survival}
\end{displayquote}

It is obvious that it depends on one's current hydration and body size.
A well hydrated person can, of course, swallow one mouthful of seawater without harm.
However, when considering that the goal of consuming seawater is to increase the amount of body water, it should be remembered that the salt content of seawater prevents this.
Stated clearly, ``net gain of body water cannot occur by imbibing unadulterated seawater.''\supercite[pp. 59]{biology-human-survival}
If one is at risk of dehydration or is already dehydrated, absolutely no seawater should be consumed.

\index{urine!urban legends@in urban legends|(}
\triplesubsection{Drinking urine}
Urine contains waste products the body is intentionally trying to remove.
Beyond a certain level, which certainly has been crossed if dehydration is a risk, the amount of solute in urine exceeds what the kidneys\index{kidneys!dehydration@in dehydration} can process.
Thus, like the consumption of seawater, the consumption of urine leads to additional dehydration.
% TODO it would be nice to get a source on this that wasn't the US army or SAS field manuals
\index{urine!urban legends@in urban legends|)}

\index{dehydration!urban legends@in urban legends|)}
\index{urban legends!dehydration@in dehydration|)}

\index{hypovolemia|)}
\index{dehydration|)}

\section*{Minor Heat Illnesses}
\index{heat illnesses!minor|(}

Minor heat illnesses range from merely bothersome to requiring prompt assistance.

\subsection*{Heat Edema}
\index{heat edema|(}

Heat edema is swelling the feet, ankles, and hands when individuals are exposed to a hot environment.
This typically occurs within the first few days and is most common in elderly\index{geriatrics!heat illnesses@in heat illnesses}, unacclimatized individuals.
Heat edema is not dangerous, and treatment is not required.
The body part may be elevated or compression socks or stockings may be used.

\index{heat edema|)}

\subsection*{Heat Rash}
\index{miliaria|see {heat rash}}
\index{sweat rash|see {heat rash}}
\index{prickly heat|see {heat rash}}
\index{skin!heat illnesses@in heat illnesses|(}
\index{heat rash|(}

Heat rash occurs when sweat glands become plugged and rupture causing a vesicle (small fluid filled sac) to form under the skin.
The rash is often located on the chest, neck, and anywhere with skin folds such as the elbow creases or under the breasts.
It may also be found in places where the body rubs against clothing including the shoulders (from backpack straps) or ankles (from tight boots).
Repeated cases of heat rash cause a deeper vesicle to form.

\index{shingles|see {herpes zoster}}

\triplesubsection{Signs and symptoms}
Heat rash (\autoref{fig:heat_rash}) is a skin disease characterized by small, red, itchy rashes that possibly include white bumps.
The affected area may not be sweaty (anhidrosis).
Heat rash is also known as miliaria, prickly heat, and sweat rash.
Heat rash is often mistaken for shingles (herpes zoster\index{herpes!zoster}).

\begin{figure}[htbp]
\centering
\caption{Heat Rash\supercite{baedr}}
\label{fig:heat_rash}
\includesvg[height=4cm, keepaspectratio]{heat-rash}
\end{figure}

\triplesubsection{Treatment}
Treatment includes topical use of calamine lotion\index{calamine} to reduce itching.
Scrubbing the affected area with salicylic acid\index{salicylic acid} can help relieve symptoms.
Symptoms typically resolve on their own when the patient is removed from a hot environment.

\index{heat rash|)}
\index{skin!heat illnesses@in heat illnesses|)}

\subsection*{Heat Cramps}
\index{heat cramps|(}

Heat cramps are painful, involuntary muscle spasms typically in the calves but may be in the shoulders or thighs.
They are most common in unacclimatized individuals who have been exercising and individuals who have been sweating profusely and replaced the lost fluid with water or other hypotonic fluids.\supercite[p. 1367]{tintinallis}
Cramps may appear during exercise, but more commonly when resting after exercise.

\triplesubsection{Treatment}
Treatment is to move the patient to a cool location and administer fluids and salts.
Fluids should be isotonic or even hypertonic.\supercite{heat-injuries-wilderness-2019}
Athletic drinks with electrolytes, especially those with sodium, potassium, and magnesium, are preferred to rehydrating with water.
Slowly and gently stretching the affected muscle helps relieve cramps.
The patient should avoid exercise after recovering to prevent a recurrent injury.

Do not massage the muscle.
This may make the cramps worse.
Do not administer salt tablets by themselves as this may cause nausea and vomiting.\supercite[p. 1368]{tintinallis}
Salt tablets may be dissolved in water then administered.

\index{heat cramps|)}

\subsection*{Heat Syncope}

% TODO may be useful to mention that classically this occurs while marching and standing at attention (military background)
Heat syncope is dizziness and fainting due to hyperthermia.
It is caused by \gls{hypotension} as a result of systemic vasodilation and the pooling the blood in the legs.

Individuals who are not acclimatized or are dehydrated are at increased risk for heat syncope.
Standing still for a prolonged periods is also a risk factor.
Individuals at risk for heat syncope should flex their legs to prevent subcutaneous blood pooling.\supercite{heat-injuries-wilderness-2019}

\triplesubsection{Signs and symptoms}
In milder forms, symptoms may simply be headache, tunnel vision, or malaise.
Patients may suddenly collapse in the heat.

\triplesubsection{Treatment}
Treatment is similar to that of heat stress (discussed later in this chapter).
If the patient fainted and fell, check their ABCs before administering other treatments.
Treat the patient as if they have heat stress.
When the patient is able drink, have them sit up, and administer isotonic fluids or water to them.
When the patient has recovered, assist them with standing.
The act of standing may cause their blood pressure to drop again as gravity causes blood to pool in their legs (orthostatic hypotension\index{orthostatic hypotension}).
Patients who have fainted should avoid strenuous exercise.

\index{hypoglycemia!heat syncope@in heat syncope|(}
\quadruplesubsection{Consider hypoglycemia}
Anecdotally, medics may find that patients with heat syncope are also mildly hypoglycemic.
This seems to be due to a general unpreparedness for demonstrating in the heat and seems to be most common in youthful patients (approximately under 25).
Patients who are unprepared usually have not eaten or drank enough during the day and as result are generally ``just not functioning well.''
Consider measuring their blood glucose and administering a small amount of sugary food or drink along with the rest of the treatment.
\index{hypoglycemia!heat syncope@in heat syncope|)}

\index{heat illnesses!minor|)}

\section*{Major Heat Illnesses}
\index{heat illnesses!major|(}

Minor heat illnesses are self correcting or do not require treatment, but major heat illnesses can be fatal if left untreated.

\subsection*{Heat Stress}

Heat stress, also known as heat exhaustion, is a syndrome of non-specific symptoms caused by heat exposure that is generally characterized by malaise.
It is itself not dangerous, but it is a precursor to heat stroke and should be preemptively treated.

Heat stress can be caused by water depletion, sodium depletion, or both.
Water depletion is typical for the elderly or those working in hot environments.
Sodium depletion is typical for unacclimatized individuals who rehydrate with hypotonic solutions.

\triplesubsection{Signs and symptoms}
Heat stress is characterized by the symptoms of heat cramps and heat syncope along with symptoms of dehydration.
In particular this means the patient may have nausea, headaches, malaise, dizziness, orthostatic hypotension, and tachycardia.
Core temperature may be normal or elevated, but is typically not above \SI{40}{\celsius}.
A key differentiation between heat stress and heat stroke is that heat stroke lacks the \acrlong{CNS} (\acrshort{CNS}) abnormalities that heat stroke has.

\subsection*{Heat Stroke}
\index{heat stroke|(}

Heat stroke is characterized by a core temperature over \SI{40}{\celsius} and an altered mental status.
Heat stroke has a high mortality rate and is fatal if left untreated.

\triplesubsection{Signs and symptoms}
Signs of heat stroke include all of the signs and symptoms of heat stress.
It is falsely believed that patients with heat stroke stop sweating, though this is true in less than half of cases.\supercite[p. 1368]{tintinallis}
The main finding that characterizes heat stroke compared to heat stress is altered mental status.
This may include impaired voluntary motor control (\gls{ataxia}), irritability, confusion, hallucinations, delirium, abnormal posturing, seizures, and coma\index{coma}.
Additionally, seizures may occur while cooling the patient.

\index{heat stroke|)}

\subsection*{Treatment of Major Heat Illness}

Neurological damage from hyperthermia is a function of maximum temperature reached as well as duration of hyperthermic temperature.\supercite[p. 1368]{tintinallis}
For this reason, it is critical to rapidly cool the patient.
Patients who are only heat stressed need to be rapidly cooled to prevent heat stroke from developing.

\triplesubsection{Remove the patient from the source of heat}
Promptly move the patient to a cooler area.
At a minimum, move them to the shade.
If possible, find a shady grass area as this will be cooler than pavement.
You may be able to use an apartment foyer or climate controlled business.
If you cannot find shade, use an emergency blanket\index{emergency blankets} held at a distance greater than \SI{1}{\meter} to create shade.

\triplesubsection{Remove excess clothing}
Loosen or remove clothing and bags to both facilitate evaporative cooling and reduce insulation.
If the patient is heat stressed, you may be able to take the time to remove the clothing carefully so they can don it later.
Use trauma shears to cut away the clothing in patients with heat stroke.

\triplesubsection{Check their blood glucose}
The patient may not be have heat illness and may be hypo- or hyperglycemic.
Check the patient's blood glucose.
If you are unable to check it, administering glucose will not worsen hyperglycemia and it will help in other cases.
Glucose should be administered only if the patient has a \acrshort{LOC} that permits it and with sufficient water to prevent worsening dehydration.

\triplesubsection{Cool the patient}
If the patient is heat stressed, passive cooling may be effective.
Douse the patient with cool water and fan them.

If the patient has heat stroke, active cooling is needed.
Cover as much of the patient's body with bags of ice as possible.
Slightly fill the bags with water to increase the surface area in contact with the body.
This increases conductive cooling.
Instant cold compresses do not cool enough to be used alone, though their use is better than nothing.
\index{urban legends!hyperthermia@in hyperthermia}
Urban legend suggests placing them along large arteries and veins cools the blood as it passes, but this is false, and it is more effective to place the bags on the palms of the hands, soles of the feet, and cheeks.\supercite{heat-injuries-wilderness-2019}

Cold water immersion therapy is the most effective method of cooling a patient in the field.
If a bath is available, fill the bath with ice water and place the patient in it ensuring to keep their head above water.
This method is unlikely to be available to medics, but is is included as getting a patient to a hospital may be impossible.

To prevent overcooling and related complications, the patient should not be cooled to a core temperature below \SI{39}{\celsius}.\supercite{heat-injuries-wilderness-2019}

\triplesubsection{Rehydrate}
If the patient is able to drink, give them isotonic fluids, or if this is unavailable, give them water.

\index{antipyretics!heat illness@in heat illness|(}
\triplesubsection{Avoid antipyretics}
Antipyretics (fever reducing medication) such as ibuprofen, aspirin, or paracetamol should not be administered.
They operate by overriding chemicals in the hypothalamus in order to lower the body's hypothalamic set-point.
When the body is overheating from an outside source, this has no effect.
\index{antipyretics!heat illness@in heat illness|)}

\triplesubsection{Consider evacuation}
If you suspect a patient has heat stroke, they should be immediately evacuated to advanced medical care.
Attempt to cool the patient while you wait for evacuation.

\index{heat illnesses!major|)}

\section*{Summary}

Many heat illnesses are merely nuisances, but heat stroke has an acute onset and high morbidity.
It requires rapid cooling and evacuation to advanced medical care.

Luckily, many heat illnesses can be prevented by appropriate clothing, hydration, and acclimatization.
Encouraging comrades to spend time in the heat prior to actions and bring plenty of water will help minimize heat illnesses.

If a patient appears to be overheating and has altered mental status, check their blood glucose.
If this is not possible, assume heat stroke and immediately evacuate them to advanced medical care.

\index{heat illnesses|)}
