\chapter{Patient Evacuation}
\label{ch:patient_evacuation}

\epigraph{Practice mutual aid! That is the surest means for giving to each and to all the greatest safety, the best guarantee of existence and progress, bodily, intellectual, and moral.}
{Pyotr Kropotkin, \textit{Mutual Aid: A Factor of Evolution}\supercite{mutual-aid}}

\index{patient evacuation|(}

\noindent
Patient evacuation is the general term used for moving a patient from somewhere dangerous to somewhere they can receive care.
This may mean moving them away from central conflict where medics can quickly treat them, or it may mean a more complicated situation of transporting a seriously wounded comrade to advanced medical care.
The nature of an evacuation depends on the patient's injury or illness, the tactical situation, and what advanced medical care is available.

Throughout this book the phrase ``\gls{advanced medical care}'' is used to describe care that can be provided by more qualified medical personnel, typically at a facility designated for caring for the ill and injured.
In the west, in everyday circumstances, this care would mean calling an ambulance that may or may not take the patient to a hospital\index{hospitals}.
Medics may not have ambulances available, and hospitals may be monitored by police, so sending a protester to a hospital can lead to their arrest\index{arrest}.
\index{hospitals!field|(}
Doctors, nurses, and \acrshort{EMT}s sometimes volunteer their time to create makeshift clinics near actions using comrades' homes or social centers as an alternative to traditional medical care.
Such field hospitals are also used in regions where healthcare is prohibitively expensive or where police repression\index{repression} can lead to arrest\index{arrest} after treatment in a traditional hospital.
Thus, an evacuation to advanced medical care might mean calling an ambulance or it could be getting a patient into a car and driving them to a field hospital.
To allow medics to make these decisions on their own, the general terms are used throughout this book.
\index{hospitals!field|)}

Further, the term ``definitive care'' is used to mean a place where a patient will receive complete care for their injuries or illness.
Definitive care implies safety from police and other fascists and enough calm that the patient can relax.
This typically means advanced medical care, but for minor injuries or when advanced medical care is not available, definitive care might mean treatment in the back of a sympathetic cafe.

The first step of evacuation is moving the patient.
Patients may be able to assist with their evacuation, or they may be fully incapacitated.
A medic may have to move a patient alone, or there may be others around to assist.
Stretchers are rarely available as most medics do not pack a carry tarp, and improvising one may be difficult and time consuming.\footnotemark[1]
The primary means of patient evacuation are various carries.

\footnotetext[1]{
Luckily, it is relatively uncommon to both need to evacuate a patient via stretcher and to have no access to ambulances.
}

\section*{Evacuation Guidelines}
\index{patient evacuation!guidelines|(}

The following are guidelines that are generally applicable for patient evacuations.
However, you may find the following does not apply in your region to you due to the nature of emergency medical services or police repression\index{repression}.

\triplesubsection{Plan for evacuation}
Before an action begins, it should be known what traditional and nontraditional medical resources are available.
The safety of traditional hospitals should be known so that medics can inform patients, and the patients can give informed consent\index{consent} to their evacuation.
For field hospitals\index{hospitals!field}, their locations and the contact information for their operators should be known as should the safest routes from an action to these facilities.
The level of care offered by field hospitals should also be known so that medics can decide if it is appropriate to send patients there.
Life threatening injuries and illnesses should always be sent to hospitals unless the patient does not consent.
An arrested protester is better than a dead protester.

\triplesubsection{Only evacuate when necessary}
Unless the scene is unsafe, it may be more suitable to leave the patient in place for initial treatment.
This is especially true of suspected spinal injuries.

\index{consent|(}
\triplesubsection{Obtain patient consent}
Patients should consent to evacuation before they are evacuated.
Implied consent\index{consent} allows you to evacuate patients who are not alert, but you should still listen a patient's requests or requests from their comrades.
Patients may not want an ambulance or to go to a hospital, or they may not want to leave an action.
These decisions should be respected, but that does not mean you should not clearly articulate the risks of forgoing advanced medical care.
\index{consent|)}

\triplesubsection{Communicate during evacuation}
Before attempting to evacuate a patient, all medics and volunteers who are assisting with the evacuation should know the evacuation route.
The situation may change, and improvisation may need to occur, but it is easier to sort out details before starting an evacuation.
Naturally, there are times where no plans can be made such as when a patient is in crossfire between protesters and riot cops.

\triplesubsection{Lift together}
If multiple medics are involved in carrying the patient, one medic should be the leader who coordinates lifting and lowering the patient.
The medic should say something similar to ``Lift on three. One, two, three.''
This prevents anyone from lifting before of after everyone else, preventing injury to medics or the patient.

\triplesubsection{Lift with your legs}
When lifting a patient, squat low and keep your back in a straight line.
Lift using your legs.
Do not lock your knees and lift using only your lower back.
This is less stable, and you may injure yourself.

\triplesubsection{Move slowly}
Moving quickly may save a few seconds, but the increased risk of dropping the patient is not worth it.
Lifting and lowering a patient should be done with slow, controlled movements.
Walking at a brisk pace is generally the fastest medics should transport a patient.

\triplesubsection{Less is more}
People like to help with evacuations because it makes them feel useful.
Generally, two medics are all that is necessary to carry a patient without a stretcher.
Untrained volunteers may be able to help with the weight, but they often get in the way and trip over each other.
Medics do not need much strength or fitness to carry patients short distances, which is often all that is necessary.

\index{ambulances!patient evacuation@in patient evacuation|(}
\triplesubsection{Ambulances}
If an ambulance was called, it may be difficult for them to find or reach you.
Unless there is immediate risk to the patient, it is generally better to leave the patient where you are treating them then to move them to the ambulance.
Send a medic or volunteer to rendezvous with the ambulance and guide the \acrshort{EMT}s to the patient.
Do not abandon the patient to find the ambulance.
\index{ambulances!patient evacuation@in patient evacuation|)}

\index{helicopters!patient evacuation@in patient evacuation|(}
\triplesubsection{Helicopters}
If patients are being evacuated by helicopter, stay back from the landing zone until signaled by the crew to approach.
Dust that is kicked up can temporarily blind you.
Rotors are hard to see and can kill you.
Do not approach the helicopter from high ground or from the rear.
Crouch low while approaching.
\index{helicopters!patient evacuation@in patient evacuation|)}

\index{patient evacuation!guidelines|)}

\section*{Carries and Drags}

Most evacuations can be accomplished by one of the following carries or drags.
\Glspl{contraindication} for these are suspected spinal injuries as none of these protect the spine.

\subsection*{Walking Assist}
\index{walking assist|seealso {patient evacuation}}
\index{walking assist|(}

A walking assist (\autoref{fig:walking_assist}) is used with patients who have enough strength and coordination to support their own weight and assist with their evacuation.
Examples are patients with a sprained ankle, patients who have been pepper sprayed, or patients who are dazed or intoxicated.
\Glspl{contraindication} for a walking assist are injuries to the patient's arm, shoulder, or ribcage.
This carry can be modified to use two medics with one on each side.

\begin{figure}[htbp]
\centering
\caption{Walking Assist\supercite{baedr}}
\label{fig:walking_assist}
\includesvg[height=6cm, keepaspectratio]{walking-assist}
\end{figure}

To perform a walking assist, stand beside the patient.
Place their arm over your shoulder and grasp their wrist with your hand.
Place your other arm around their waist.

The walking assist is often used to scoop up patients who have been pepper sprayed and cannot see.
When performing this on a patient who is blinded, announce that you are a medic who is going to help them to safety before touching them.
It is typically enough to say ``I'm a medic. Let's go!''
If a patient thinks you are a cop, they may recoil, trip, and hurt themself, or they may strike at you.

\index{walking assist|)}

\subsection*{Two-Person Carry}
\index{two-person carry|seealso {patient evacuation}}
\index{two-person carry|(}

A two-person carry (\autoref{fig:two_person_carry}) is used with patients who cannot walk on their own but are alert enough to keep balance.
Contraindications are injuries to the patient's arms and shoulders that prevent them from helping support themself.
While it is possible to use this carry with an unconscious patient, other carries are preferred.

\begin{figure}[htbp]
\centering
\caption{Two-Person Carry\supercite{baedr}}
\label{fig:two_person_carry}
\includesvg[width=\textwidth, height=6cm, keepaspectratio]{two-person-carry}
\end{figure}

To perform a two-person carry, two medics face each other.
They extend one arm onto to each other's shoulder to create a back support.
Using their other arms, they grasp one another's wrists to create a seat.
If it is difficult for the medics to grasp each other's wrists (possibly because of sweat or bulky clothing), a loop of cloth (such as a gaiter) or a belt can be used to create a ``seat'' that both medics grasp.
The medics squat low while the patient leans back into the medics' arms.
The patient places their arms over the medics' shoulders.
This is necessary to prevent the patient from falling forward while being carried.

\index{two-person carry|)}

\subsection*{Rautek Maneuver}
\index{Rautek maneuver|(}

The Rautek maneuver\footnotemark[2] (\autoref{fig:rautek_maneuver}) can be used by a single medic.
Most medics can perform this alone on patients who are far heavier than them to move a patient tens of meters.
This is the most complicated of the carries and drags, but it is the safest and most stable.
Contraindications are injuries to the arm and shoulder.

\footnotetext[2]{
The Rautek maneuver (German: \textit{Rautek-Rettungsgriff}) is named after its inventor, Franz Rautek, an Austrian martial arts instructor.\supercite{notfallmedizin}
}

\begin{figure}[htbp]
\centering
\caption{Rautek Maneuver\supercite{baedr}}
\label{fig:rautek_maneuver}
\includesvg[height=6cm, keepaspectratio]{rautek-maneuver}
\end{figure}

In this example, it is assumed that the medic is right hand dominant and that the patient has an uninjured right arm and shoulder.
To perform the Rautek maneuver, use the following steps:

\begin{enumerate}
    \item Roll the patient on to their back.
    \item Kneel behind the patient's head.
    \item Lift the patient's head into your lap and scoot your knees under their shoulders.
    \item Push the patient up into a sitting position so that their shoulders are supported by your chest.
    \item Cross their right arm over their chest.
    \item Using your left arm, reach under their left arm and grasp their right wrist.
    \item Using your right arm, reach under their right arm and grasp their right forearm.
    \item Lift their torso off the ground and walk backwards so that their feet drag.
\end{enumerate}

When you have reached your destination, perform the steps backwards so that the patient's torso is slowly lowered on to the ground.
Take care to ensure their head does not hit the ground as you lower them.

This maneuver can be modified into a carry with assistance from a second medic.
The second medic lifts the patient's feet while the first medic lifts the patient's torso.
This maneuver can also be modified by grasping your hands together in front of the patient instead of using their arm as a support.

\index{Rautek maneuver|)}

\subsection*{Blanket Drag}
\index{blanket drag|seealso {patient evacuation}}
\index{blanket drag|(}

The blanket drag (\autoref{fig:blanket_drag}) is the least complex for a single medic to use on a heavy patient.
Of the carries and drags, it is also the least harmful for a patient with a suspected spinal injury.
However, use of a stretcher is preferred, and waiting for an ambulance is ideal.

\begin{figure}[htbp]
\centering
\caption{Blanket Drag\supercite{baedr}}
\label{fig:blanket_drag}
\includesvg[height=6cm, keepaspectratio]{blanket-drag}
\end{figure}

To perform a blanket drag, place the patient on their back next to on a blanket, tarp, or canvas banner.
Roll them into the rescue position\index{rescue position!patient evacuation@in patient evacuation}, slide the blanket up against them, then roll them back on to it.
The blanket should extend above their head to provide support.
Grasp the corners of the blanket above the patient's head using both hands.
Drag the patient while walking backwards.
When you have reached your destination, lower the blanket gently to avoid hitting the patient's head against the ground.

If a blanket is not available, this drag can be modified by grasping the clothing on the patient's shoulder and using your wrists to provide support for the patient's head.
However, the Rautek maneuver is preferred.

\index{blanket drag|)}

\section*{Stretchers}
\index{stretchers|seealso {patient evacuation}}
\index{stretchers|(}

Medics who wish to evacuate patients via a stretcher would ideally use a carry tarp.\footnotemark[3]
However, these are may be too heavy and large for your medic bag.
Stretchers can be improvised using materials available at an action.

\footnotetext[3]{
Use of commercial, foldable stretchers is not recommended for most medics.
They are too large, heavy, and cumbersome to carry during actions in the offhand chance they are needed.
If you have unofficial ambulance services, such as a comrade with a station wagon, use of commercial stretchers may be appropriate.
}

\subsection*{Stretcher Usage}
\index{stretchers!usage@usage of|(}

Generally, only two medics are required to carry a patient on a stretcher, though their shape allows four to six medics to assist in carrying a patient.
Regardless of the stretcher used, several medics are needed to get a patient onto a stretcher.

Patients should be rolled onto their side as described in \fullref{ch:brain_and_spinal_cord_injuries}.
Once they are on their side, a stretcher can positioned next to them, and the patient can be rolled back on to it.
\index{stretchers!usage@usage of|)}

\subsection*{Improvised Stretchers}

There are a number of ways to improvise stretchers.
The following subset of methods are provided because they are simple and reliable.

Stretchers should be longer than the patient is tall.
If this is not possible, patients without knee, lower leg, or spinal injuries can be transported in shorter stretchers with their legs hanging off the edge.

\subsubsection*{Tarp Stretcher With Poles}

A stretcher can be improvised using a tarp or canvas banner and two poles (\autoref{fig:tarp_stretcher_with_poles}).
The tarp needs to be approximately longer than the patient is tall and roughly three times as wide.
The poles need to be longer than the tarp.

\begin{figure}[htbp]
\centering
\caption{Tarp Stretcher With Poles\supercite{anon-3}}
\label{fig:tarp_stretcher_with_poles}
\includesvg[width=\textwidth, height=6cm, keepaspectratio]{tarp-stretcher-with-poles}
\end{figure}

Place the tarp on the ground and place one pole approximately one third the distance from one of the sides.
Fold the tarp over the pole so that the edge is slightly past the other one third marker.
Place a second pole over this one third marker.
Fold the remaining edge of the tarp back over the first pole.

The tarp will be held in place by the weight of the patient on the tarp and the static friction of the tarp against itself and the poles.
The tarp can be additionally secured by long strips of duct tape on the fold perpendicular to the direction of the poles.

\subsubsection*{Tarp Stretcher Without Poles}

A stretcher can be improvised using just a tarp (\autoref{fig:tarp_stretcher_without_poles}).
Place the patient on the tarp and tightly roll the tarp toward the patient so that a ``handle'' is created.

\begin{figure}[htbp]
\centering
\caption{Tarp Stretcher Without Poles\supercite{anon-3}}
\label{fig:tarp_stretcher_without_poles}
\includesvg[width=\textwidth, height=6cm, keepaspectratio]{tarp-stretcher-without-poles}
\end{figure}

\subsubsection*{Jacket Stretcher}

A jacket stretcher can be created using jackets and two poles (\autoref{fig:jacket_stretcher}).
Jackets are preferred to sweaters or t-shirts because they are made of more durable materials.
If t-shirts are used, they should be doubled or tripled up to prevent tearing.

\begin{figure}[htbp]
\centering
\caption{Jacket Stretcher\supercite{anon-3}}
\label{fig:jacket_stretcher}
\includesvg[width=\textwidth, height=6cm, keepaspectratio]{jacket-stretcher}
\end{figure}

Zip up the jackets and fasten all buttons.
Tuck any hoods into the body of the jacket to prevent them from catching on anything while using the stretcher.
Tuck the sleeves of the jackets inward through the body of the jacket.
Slide the poles through the sleeves of the jackets.

\index{stretchers|)}

\section*{Protecting Patients During Evacuation}
\index{anonymity!patients@of patients|(}

During patient evacuation, it is important to protect patients.
This usually only means protecting them from arrest\index{arrest} and identification during evacuation.
Journalists\index{journalists!patient privacy@in patient privacy} may try to film or photograph patients for their story, and fascists may try to do the same for the purposes of doxxing\index{doxxing}.
In some regions, police film protesters to use later in prosecution for rioting or property destruction.

\index{legality|(}
During evacuation, fully wrap patients in emergency blankets\index{emergency blankets!patient evacuation@in patient evacuation} or a tarp to hide their clothing.
Place a surgical mask \index{emergency blankets!patient evacuation@in patient evacuation}over their face to hide most of their facial features.
In some regions, face coverings are illegal during protests, but a surgical mask can be described as medically necessary and police may tolerate this more than a bandana.
Place a hat, beanie, or bandana over the patient's head to hide their hair.
While moving them, other medics or volunteers can hold a canvas banner or emergency blankets between the patient and others to help hide them (\autoref{fig:riot_cops_with_camera}).
This can typically be done on the legal basis of ensuring patient confidentiality\index{patient-caregiver confidentiality}.\footnotemark[4]
\index{legality|)}

\footnotetext[4]{
As anarchists, we should not place faith in the law or legal institutions, but so long as they exist, we can leverage them against the State to protect patients.
}

\begin{figure}[htbp]
\centering
\caption{Riot Cops with Camera\supercite{baedr}}
\label{fig:riot_cops_with_camera}
\includesvg[width=\textwidth, height=7cm, keepaspectratio]{riot-cops-with-camera}
\end{figure}

\index{anonymity!patients@of patients|)}

\section*{Summary}

What constitutes patient evacuation to advanced medical care depends on the exact context in which a medic is acting.
This may mean a hospital or simply a temporary space designated for medical treatment.
Evacuation procedures should be agreed upon by medics before an action, but they must also take into account patient consent.
Medics should prefer to treat a patient in place unless there is risk to the patient.
Lifts and drags should be coordinated so that all medics move as one.
Medics should remember to move slowly as moving too quickly will likely do more harm than good.

\index{patient evacuation|)}
