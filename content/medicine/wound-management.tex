\chapter{Wound Management}
\label{ch:wound_management}

\epigraph{
In Egyptian mythology the eye of Horus was an ancient symbol of protection and power to ward off evil.
I should hope that my eye can become a beacon of hope that supports Hong Kongers to ward off evil.
}{Anonymous Hong Kong Medic\supercite{hong-kong-medic-eye-injury}\textsuperscript{,}\footnotemark[1]}

\footnotetext[1]{
\index{Hong Kong!2019--20 protests}
On August 11th, 2019, a medic had her eye shot out by a bean bag round fired by police during the anti-ELAB protests in Hong Kong.
Her injury became a symbol for her fellow protesters leading to demonstrations in support of her.
This quote was from her first public statement after her injury.
}

\index{trauma!wounds|see {wounds}}
\index{wounds|(}

\noindent
Treating wounds is what people often think of when they hear the words ``first aid.''
Sometimes wound management is as simple as putting ice on a bruise or stopping minor bleeding on a finger.
At an action, these injuries may be caused by accidents, but often they are caused by State violence against protesters.
Much of your work as a medic will be treating wounds, and while this book teaches you the basics of bandaging wounds, you will still need to practice with with others.

\begin{figure}[htbp]
\centering
\caption{Abrasion Example\supercite{audrey}}
\includesvg[width=4cm, keepaspectratio]{abrasion-example}
\end{figure}

\section*{Physiology}
\index{skin!physiology@physiology of|(}
The skin is composed of two layers (the epidermis and dermis) with subcutaneous tissue beneath the dermis.
The epidermis is the outermost layer.
It is thin and has no blood vessels.
The epidermis protects the body from pathogens and regulates water loss.
The dermis is the next layer down.
It composed of dense connective tissue and cushions the body.
The dermis also houses blood vessels, nerves, hair follicles, sweat glands, and sebaceous glands (oil glands) among other structures.
Below is the subcutaneous tissue that attaches the skin to the underlying bone and muscle.
It is 50\% fat and contains more nerves and blood vessels than the dermis.
See \autoref{fig:skin_cross_section} for a cross section of the skin.
\index{skin!physiology@physiology of|)}

\begin{figure}[htbp]
\centering
\caption{Skin Cross Section\supercite{anon-3}}
\label{fig:skin_cross_section}
\includesvg[width=\textwidth, height=6cm, keepaspectratio]{skin-cross-section}
\end{figure}

\index{hemostasis|(}
Hemostasis is the process of stopping bleeding through natural or artificial means.
The human body naturally maintains hemostasis while bleeding through vasoconstriction and the coagulation of blood\index{blood!coagulation@coagulation of}.
If a patient is hemorrhaging, their body may not be able to achieve hemostasis before they die of blood loss (exsanguination).
\index{hemostasis|)}

\section*{Wound Classification}
\index{wounds!classification@classification of|(}

Traumatic wounds can be divided into blunt force trauma and penetrating trauma.
As the name suggests, penetrating trauma is caused by an object penetrating the body such as a puncture from stepping on a nail, a knife wound, or a gunshot wound.
Blunt force trauma is caused by impact such a being struck by a fist or police baton.
Wounds are also classified as either open or closed based on whether or not the skin was broken.

\index{wounds!classification@classification of|)}

\section*{Closed Wounds}
\index{wounds!closed|(}

Closed wounds come in two main varieties: hematomas and crush injuries.
Internal bleeding is also a closed wound, such as bleeding following a bone fracture of damage to the lungs.
Treatment for internal bleeding\index{internal bleeding} is covered by the chapters relevant to the original injury.

\subsection*{Hematomas}
\index{bruises|see {contusions}}
\index{contusions|seealso {hematomas}}
\index{hematomas|(}

A hematoma is a collection of blood outside of the blood vessels.
This may be pooling of blood in a body cavity or space between two tissues such as between the dermis and a finger or toenail.
Hematomas may form at the site of an injury or blood may move under loose tissue such as in the case blood pooling in the foot beneath the skin following a sprained ankle.

A contusion\index{contusions}, more commonly known as a bruise, is a type of hematoma characterized by the leakage plasma or blood into the surrounding tissue caused by damaged capillaries.

Hematomas and contusions are characterized by localized pain, swelling, and discoloration.

\subsubsection*{Treatment}
\index{hematomas!treatment@treatment of|(}

Apply ice or a cold-pack to minimize swelling.
Check for fractures beneath the bruises.
With more painful hematomas, a limb may need to be immobilized to minimize pain.

After the first 12 hours, the patient may use warm compresses on a hematoma to increase blood flow and facilitate reabsorption of fluid from the tissues.
For some large hematomas, a patient may need to have the blood drained by advanced medical care.

\index{hematomas!treatment@treatment of|)}
\index{hematomas|)}

\subsection*{Crush Injuries}
\index{kidneys|seealso {renal failure}}
\index{crush injuries|(}

Crush injuries are caused by compression of the body and are often the result of automobile accidents or building collapse.
The may be accompanied by other traumatic injuries on or near the site of the crush injury itself.
Crush injuries can cut off circulation to body parts and cause massive damage to muscles.
The breakdown of damaged muscle tissue (rhabdomyolysis\index{rhabdomyolysis}) can release toxins into the blood.
Complications include hypovolemic shock\index{shock!hypovolemic!crush injuries@in crush injuries}, compartment syndrome\index{compartment syndrome!crush injuries@in crush injures}, acute renal failure\index{renal failure!crush injuries@in crush injures}, and cardiac arrest\index{cardiac arrest!crush injuries@in crush injures}.

% TODO cover rhabdomyolysis more in depth as this can be caused by being severely beaten, so it's relevant to "combat injuries" too

\subsubsection*{Treatment}
\index{crush injuries!treatment@treatment of|(}

Crush injuries to the \gls{distal} end of an extremity may not be life-threatening, but prompt medical care is require to retain use of the limb.
Other crush injuries, especially to the upper leg, are medical emergencies.
In all cases, check the patient's ABCs, control bleeding, and treat for shock.
Evacuate the patient to advanced medical care.

\index{crush injuries!treatment@treatment of|)}
\index{crush injuries|)}
\index{wounds!closed|)}

\section*{Open Wounds}
\index{skin!trauma|see {wounds}}
\index{wounds!open|(}

Open wounds are classified according mechanism of injury.

\begin{figure}[htbp]
\caption{Open Wounds\supercite{audrey}}
\centering
\begin{subfigure}[b]{4.5cm}
    \centering
	\includesvg[width=\textwidth, height=4cm, keepaspectratio]{abrasion}
	\caption{Abrasion}
\end{subfigure}
\begin{subfigure}[b]{4.5cm}
    \centering
	\includesvg[width=\textwidth, height=4cm, keepaspectratio]{laceration}
	\caption{Laceration}
\end{subfigure}
\begin{subfigure}[b]{4.5cm}
    \centering
	\includesvg[width=\textwidth, height=4cm, keepaspectratio]{puncture-wound}
	\caption{Puncture}
\end{subfigure}
\begin{subfigure}[b]{4.5cm}
    \centering
    \includesvg[width=\textwidth, height=4cm, keepaspectratio]{avulsion}
    \caption{Avulsion}
\end{subfigure}
\end{figure}

\index{scrapes|see {abrasions}}
\index{road rash|see {abrasions}}
\index{rug burns|see {abrasions}}
\index{abrasions|seealso {open wounds}}
\index{abrasions|(}
\triplesubsection{Abrasions}
Abrasions are superficial wounds where the epidermis is scraped off.
An abrasion is commonly called a scrape, road rash, or rug burn.
Abrasions bleed minimally or not at all.
Abrasions heal faster when they are kept moist with an antibacterial ointment and covered with a dressing.
\index{abrasions|)}

\index{lacerations|seealso {open wounds}}
\index{incisions|seealso {open wounds}}
\index{stitches|see {sutures}}
\index{lacerations|(}
\index{incisions|(}
\triplesubsection{Incisions and lacerations}
Incisions are wounds caused by cutting by a sharp object such as a knife or glass.
The edges of an incision are straight.
Incisions are often miscategorized as lacerations.

Lacerations are wounds that are caused by tearing of the skin by blunt force trauma such as skin splitting after being hit by a baton of after falling on pavement.
The edges of a lacerations are jagged.

Incisions and lacerations may be relatively superficial or may penetrate all the way to muscle or other underlying tissue.
Incisions and lacerations require sutures\index{sutures} (stitches) from advanced medical care if they are long or deep enough to cause the skin to gap.
Sutures are also recommended for cosmetic reasons\footnotemark[2] and in order to help retain full function if the injury is located on the hands, feet, or face.

\footnotetext[2]{
\index{autonomy!bodily}
When advanced medical care is recommended on cosmetic grounds, it should noted that the recommendation comes not as a means of promoting a singular type of universal beauty.
It is mentioned so that you can inform the patient that failure to seek additional care may lead to a permanent and obvious change to their appearance.
This is something they may not want, or they may not care.
It is your duty to inform them of this possibility so that they may make their own decision.
}

\index{lacerations|)}
\index{incisions|)}

\index{puncture wounds|see {wounds, puncture}}
\index{wounds!puncture|(}
\triplesubsection{Puncture wounds}
A puncture wound is a wound caused by an object piercing the body such as a nail or sharp piece of wood.
Puncture wounds may bleed only minimally due to the object acting as a plug.
However, deeper tissues and organs may be damaged including major arteries or veins.

Impaled objects should be left in place and stabilized with gauze until they can be removed by advanced medical care.
If the impaled object is loose enough to fall out on its own or if it prevents transport, it may be removed.
Impaled objects to the cheek, face, or neck that cause airway obstruction may need to be removed to allow the patient to breathe or to facilitate CPR.
\index{wounds!puncture|)}

\index{avulsions|(}
\triplesubsection{Avulsions}
An avulsion is the tearing away of a body part.
This may nearly amputate an entire limb or create a flap of skin as tissues tear along anatomical planes such as between subcutaneous tissue and muscle.
Small avulsions may be treated by cleaning the wound, including under the skin flap, and repositioning the skin before bandaging it.
An avulsion larger than \SI{5}{\cm} in diameter may require a skin graft to heal.
Patients with large avulsions should be evacuated or sent to advanced medical care.

\begin{figure}[htbp]
\centering
\caption{Avulsion Example\supercite{audrey}}
\includesvg[width=4cm, keepaspectratio]{avulsion-example}
\end{figure}

\index{wounds!degloving|seealso {wounds, avulsions}}
\index{wounds!degloving|(}

\quadruplesubsection{Degloving}
Degloving is a type of avulsion where a large piece of skin is torn off the underlying tissue.
The name reflects the appearance of the injury which resemble a glove being pulled off a hand.
Jewelry such as rings and bracelets can get caught when using one's hands and tear back large amounts of skin.\footnotemark[3]

\index{wounds!degloving|)}
\index{avulsions|)}

\footnotetext[3]{
This is why medics (and everyone, for that matter) are suggested to remove jewelry (especially on the hands) before an action\index{wounds!degloving}.
}

\index{amputations|(}
\triplesubsection{Amputations}
An amputation is the complete severance of a body part, usually an extremity.
Amputations of an extremity may sever a major artery and cause severe bleeding.
However, the tissues of the stump itself may only bleed minimally as the blood vessels contract.
Use of a tourniquet may be necessary to control bleeding\index{tourniquets!amputations@in amputations}.
\index{amputations|)}

\subsection*{Hemorrhage Classification}
\index{blood!loss|see {hemorrhages}}
\index{hemorrhages|seealso {hypovolemia}}
\index{hemorrhages!classification@classification of|(}

A classification system for hemorrhage can be found in \autoref{tab:hemorrhage_classification}.
This table assumes the patient has the average \SI{5}{\liter} of pre-injury circulating blood.
These values are not absolute since patients respond differently to blood loss.

Class I hemorrhage may be managed without fluid resuscitation and is not often accompanied by changes to vital signs.
Class III hemorrhage is approximately when hypovolemic shock occurs\index{shock!hypovolemic!hemorrhages@in hemorrhages}.

\begin{table}[htbp]
\centering
\footnotesize
\caption{Hemorrhage Classification Based on Estimated Blood Loss\supercite[p. 1686]{tintinallis}}
\label{tab:hemorrhage_classification}
\begin{tabular}{l|l|l|l|l|}
                                & \textbf{Class I}    & \textbf{Class II} & \textbf{Class III} & \textbf{Class IV} \\
    \hline
    Blood loss (mL)             & $<750$              & 750--1500         & 1500--2000         & $>2000$           \\
    \hline
    Blood loss (\%)             & $<15$               & 15--30            & 30--40             & $>40$             \\
    \hline
    Heart rate (\acrshort{BPM}) & $<100$              & 100--120          & 120--140           & $>140$            \\
    \hline
    Blood pressure              & Normal              & Normal            & Decreased          & Decreased         \\
    \hline
\end{tabular}
\end{table}

\index{hemorrhages!classification@classification of|)}

\subsection*{Estimating Blood Loss}
\index{hemorrhages!estimating blood loss|(}

To help you estimate if a patient has a Class II or above hemorrhage, the following can be used as references.
Using these estimations can help you determine if the person needs immediate evacuation to advanced medical care due to blood loss.

A pool of blood on a hard surface that is \SI{50}{\cm} in diameter is approximately \SI{500}{\mL}.
Pools that are \SI{75}{\cm} and \SI{100}{\cm} in diameter are approximately \SI{1000}{\mL} and \SI{1500}{\mL} respectively.
One-third of the front of a T-shirt that is soaked (not just wet) with blood is approximately \SI{250}{\mL}.

Another method for estimating blood loss is the MAR method.\supercite{mar-method}\textsuperscript{,}\footnotemark[4]
Hold your closed fist just above the pool of blood.
The area beneath is approximately \SI{20}{\mL} of blood.
The amount of blood under your fist is higher on dirt or carpet than a smooth surface.
Using this method, the upper limit of Class I hemorrhage on a smooth surface is 36 fists worth of blood.

\footnotetext[4]{
MAR is an acronym of the authors' names.
}

\index{hemorrhages!estimating blood loss|)}

\subsection*{Treatment}

% TODO need to clarify when to use what kinds of dressings
% TODO move some content form "dressings" in equipment to here

Treating open wounds consists of controlling bleeding, cleaning the wound, and dressing the wound.
Open wounds carry risk of infection\index{infection}, so patients will have to be aware of the signs so that they may seek advanced medical care on their own.

\begin{figure}[htbp]
\centering
\caption{Puncture Wound Example\supercite{audrey}}
\includesvg[width=4cm, keepaspectratio]{puncture-wound-example}
\end{figure}

Remember to practice good \acrshort{BSI}.
Always wear examination gloves to prevent coming into direct contact with a patient's bodily fluids.
You may need to wear eye and face protection when treating patients who are severely hemorrhaging.

\triplesubsection{Control bleeding}
Controlling bleeding is the first step to treating an open wound.
Patients who are bleeding from an artery may die within minutes if bleeding is not controlled.
Arterial bleeding is characterized by blood that spurts with each heartbeat whereas venal and capillary bleeding has a regular flow.
Patients may have arterial bleeding without spurts if the artery itself is not exposed such as in a stab wound.

Additionally, minimizing blood loss also helps calm both patients and bystanders as many people are sensitive to the sight of blood.
A pool of blood or large amounts of blood splatter may cause the patient or bystanders to panic or faint.

\quadruplesubsection{Apply direct pressure}
Place gauze over the wound, and use your hand to apply pressure directly to the wound.
Pack large wounds or gaping wounds with gauze before applying pressure.
Wounds with arterial bleeding may need to be packed with hemostatic gauze\index{hemostatic agents} in order to rapidly induce hemostasis\index{hemostasis}.

\index{dressings|(}
In absence of gauze or proper dressings, a bandana or other cloth may be used as a dressing.
Use of improvised dressings carries increased risk of infection.
They are not recommended except in emergencies.
If no dressing are available, apply pressure with just your hand.

If the wound bleeds through the dressing, place additional dressings over the original dressing.
Removing the original dressing may disturb a clot that is forming and slow the clotting process.

\index{dressings|)}

\quadruplesubsection{Elevate the wound}
If there are no complicating or painful injuries such as fractures or soft tissue damage that would be exacerbated by movement, elevate the limb above the heart.

\index{dressings!pressure|(}
\quadruplesubsection{Use a pressure dressing}
Using a pressure dressing will free you to attend to other injuries, begin basic life support\index{basic life support!wound management@in wound management}, or move on to other patients.
Pressure dressings can be purchased (combat dressing\index{dressings!combat}), or they can be improvised using basic medic supplies.

It is important to keep pressure on the wound while applying a pressure dressing.
Additionally, pressure dressings should not be tied too tight that that act as a tourniquet and cut off blood flow below the injury.
Pressure dressings should only be used on extremities as using them on the torso can inhibit respiration and use on the neck can cut off blood flow to the brain.

To create a pressure dressing (\autoref{fig:pressure_dressing}), place a bulky dressing (a thick gauze pad or a rolled up gauze roll) directly over the wound.
Use a gauze roll or folded triangle bandage wrapped around the extremity to secure the bulky dressing in place.
Ensure that when wrapping the extremity, the bulky dressing is fully covered so it cannot slip out.
Tie a knot directly over the wound.

\begin{figure}[htbp]
\centering
\caption{Pressure Dressing\supercite{bizhan}}
\label{fig:pressure_dressing}
\includesvg[width=\textwidth, height=5cm, keepaspectratio]{pressure-dressing}
\end{figure}

To tie a knot using a gauze roll, tear the gauze lengthwise down the middle.
Wrap each half in opposite directions around the extremity so they meet directly over the wound.
Tie a knot.
Using medical tape to affix the gauze will not provide nearly as much pressure as tying a knot.

\index{dressings!pressure|)}

\quadruplesubsection{Occlude arterial blood flow}
If pressure and elevation are not sufficient to stop blood flow, you may need to occlude arterial blood flow by using pressure points\index{pressure points} or a tourniquet\index{tourniquets}.
These two topics are discussed in their own sections later in this chapter.

\index{wounds!irrigation@irrigation of|see {irrigation}}
\index{irrigation|(}
\triplesubsection{Clean the wound}
Once the bleeding has stopped, you will need to clean the wound to prevent infection.
Flush the wound using water, saline, or specialized wound irrigation solution.
If you have an irrigation kit, use the syringe to irrigate the wound.
Spray water directly into the wound with enough pressure to dislodge debris.

If you do not have a irrigation solution, use tweezers to remove debris and dead tissue.
If you do not have tweezers, use dry gauze to remove debris and debride\index{debridement}\index{wounds!debridement@debridement of} (scrape away) the dead tissue.
Flush the wound again after cleaning.

You may need to control bleeding again as cleaning may dislodge recently formed blood clots.

\begin{figure}[htbp]
\centering
\caption{Laceration Example\supercite{audrey}}
\includesvg[width=4cm, keepaspectratio]{laceration-example}
\end{figure}

\index{irrigation|)}

\index{wounds!dressing@dressing of|(}
\triplesubsection{Dress the wound}
Apply antiseptic cream or spray to the wound, then cover it with fresh dressings.
Use a fresh dressing to cover the wound, and secure it in place using rolled gauze, tape, or a net dressing.
When securing the dressing, ensure you have not blocked blood flow to an extremity.
Check the patient's \acrshort{CSM}.

\index{wound closure strips|(}
\quadruplesubsection{Apply wound closure strips}
If the wound is gaping and no longer bleeding, you can close the wound using wound closure strips (\autoref{fig:wound_closure_strip_application}).
Wound closure strips are thin strips of porous medical tape used to hold minor wounds closed.
Ideally this wound be done by medical professionals, but such care may not be available.
Note that if you use wound closure strips, the wound must first be fully cleaned and irrigated to prevent infection\index{infections!wound management@in wound management}.

\begin{figure}[htbp]
\centering
\caption{Wound Closure Strip Application\supercite{bizhan}}
\label{fig:wound_closure_strip_application}
\includesvg[width=\textwidth, height=6cm, keepaspectratio]{wound-closure-strip-application}
\end{figure}

To apply wound closure strips, first clean and dry the area surrounding the wound.
Remove one strip from the package and press it on to the skin on one half of the wound.
Using your other hand, pinch the wound together to close it, then press the remaining half of the strip against the skin on the other side of the wound.
Every \SI{3}{\mm}, repeat this process to fully close the wound.
Optionally, after closing the wound, place additional strips parallel to the wound to reduce tension.
If possible, cover with Tegaderm or similar transparent film dressing; this will help secure the wound closure strips in place, while still allowing visual inspection of the wound.
\index{wound closure strips|)}

\index{wounds!dressing@dressing of|)}

\index{shock!wound management@in wound management|(}
\triplesubsection{Consider treating for shock}
Patients who have traumatic injuries or significant blood loss may panic and go into shock.
Patients with significant blood loss should be treated for shock, and other patients may need to be monitored for shock depending on the severity of their wounds.
\index{shock!wound management@in wound management|)}

\index{amputations!wound management@in wound management|(}
\triplesubsection{Locate amputated body parts}
Amputated body parts may be surgically reattached.
After attending to the patient, find the amputated body part and clean it with water or saline.
Wrap the body part with moist (not wet) gauze.
Place the wrapped body part in a plastic bag, and place it in cold water or atop ice.
Do not bury the body part in ice, submerge it in ice-water, or let it come in direct contact with ice (even through a plastic bag).
Ensure the body part accompanies the patient to advanced medical care.
\index{amputations!wound management@in wound management|)}

% TODO discuss extended healing such as changing bandages
% TODO infection identifying / etc.

\subsection*{Occluding Arterial Blood Flow}

Severe hemorrhaging may need to be treated by directly occluding arterial blood flow.
Two methods for doing this are use of pressure points and use of tourniquets.
These methods may be combined.
For example, one medic may immediately apply pressure to a pressure point while a second medic prepares and applies a tourniquet.

\subsubsection*{Pressure Points}
\index{pressure points|(}

Pressing directly on an artery \gls{proximal} to the heart will reduce blood flow and slow bleeding.
\autoref{fig:pressure_points} shows a subset of pressure points.
Use of pressure points should be limited to 10 minutes due to the risk of complications.
Bleeding that requires use of a pressure point is serious enough to require advance medical care.

\begin{figure}[htbp]
\centering
\caption{Pressure Points\supercite{bizhan}}
\label{fig:pressure_points}
\includesvg[width=\textwidth, keepaspectratio]{pressure-points}
\end{figure}

\index{pressure points|)}

\subsubsection*{Tourniquets}
\index{tourniquets|(}

A tourniquet is a device that applies external compression to stop blood flow, but their use is not without risk.
If you apply a tourniquet, you must not remove it.
You must get the patient to advanced medical care.
Use of a tourniquet can cause rhabdomyolysis\index{rhabdomyolysis}, and releasing the tourniquet can cause acute renal failure\index{renal failure!tourniquets@in tourniquets}.
This is a life-threatening medical condition.
Thus, use of tourniquets should be used as a last resort.\footnotemark[5]

\footnotetext[5]{
This is especially true if there is risk of arrest\index{arrest} at hospitals\index{hospitals} of in regions without affordable healthcare.
Putting a tourniquet on a patient and forcing them to go to a hospital is potentially thrusting financial burden or increase risk of arrest on them.
}

When applying a tourniquet, if you only apply enough pressure to occlude blood flow in the veins but not the arteries, the hemorrhage will worsen.
This is because there will be enough blood pressure for blood to flow through the arteries to the wound but not enough for the blood to escape.
It is critical to get enough pressure.

To apply a tourniquet (\autoref{fig:tourniquet_application}), wrap the strap around the extremity immediately \gls{proximal} to the wound and pull the strap snug.
Use the windlass to increase the pressure until the bleeding stops.
Tourniquet application is painful to the patient, but you must keep tightening it until the bleeding stops.
A rule of thumb is one complete rotation beyond when the patient winces from pain.

\begin{figure}[htbp]
\centering
\caption{Tourniquet Application\supercite{bizhan}}
\label{fig:tourniquet_application}
\includesvg[height=6cm, keepaspectratio]{tourniquet-application}
\end{figure}

Patients may try to remove tourniquets because of the pain caused by the pressure of the tourniquet and the dying muscle tissue.
This is especially true in a mass casualty incident where you may need to apply a tourniquet and move to another patient.
Explicitly tell them that removing the tourniquet is life-threatening, and consider instructing one of their comrades to prevent them from doing so.

\index{tourniquets|)}
\index{wounds!open|)}

\section*{Urban Legends}
\index{wounds!urban legends@in urban legends|(}
\index{urban legends!wound management@in wound management|(}

Because of historic use and misunderstanding, some medics use wound treatments that are potentially harmful and are not backed by science.
The following are listed for educational purposes along with explanations for why they are not permissible treatments.
None of these treatments should be used on patients.

\index{urban legends!hydrogen peroxide@in hydrogen peroxide!wound management@in wound management|(}
\index{hydrogen peroxide!urban legends@in urban legends!wound management@in wound management|(}
Hydrogen peroxide is a historic method of disinfecting wounds, and it remains a popular treatment for wounds.
Using hydrogen peroxide on a wound causes tissue death that slows wound healing and increases scarring.\supercite{hydrogen-peroxide-wounds}
Do not use hydrogen peroxide to clean or disinfect a wound.
\index{hydrogen peroxide!urban legends@in urban legends!wound management@in wound management|)}
\index{urban legends!hydrogen peroxide@in hydrogen peroxide!wound management@in wound management|)}

\index{Piper nigrum|see {pepper, black}}
\index{black pepper|see {pepper, black}}
\index{Capsicum annum|see {pepper, cayenne}}
\index{cayenne pepper|see {pepper, cayenne}}
\index{Piper marginatum|see {pepper, marigold}}
\index{marigold pepper|see {pepper, marigold}}

\index{pepper!urban legends@in urban legends|(}
\index{urban legends!pepper@in pepper|(}
\index{paraherbalism!wound management@in wound management|(}

Remedies for hemorrhage control that are frequently used by street medics, preppers, survivalists, and even lay people are black pepper\index{pepper!black} (Piper nigrum) and cayenne pepper\index{pepper!cayenne} (Capsicum annuum).
These are put directly into the wound either as a powder or by treating gauze with powder and then using the gauze to pack or cover wounds.
Proponents of these remedies have different theories about why these remedies work ranging from cauterization from the ``heat'' to increased adrenal response leading to vasoconstriction.
Some theories are not based on science while others sound plausible.
Regardless, neither cayenne nor black pepper have hemostatic properties.
Cayenne pepper does not decrease bleeding either when ingested or applied directly to the wound.\supercite{cayenne-pepper-hemostatic}
Donna Di Michele, the Deputy Director of the Division of Blood Diseases and Resources at the National Heart, Lung, and Blood Institute said of this claim ``I'm unaware of any scientific data to support the claim that cayenne pepper can stop bleeding.''\supercite{cayenne-pepper-hemostatic}
This urban legend may persist in part because the marigold pepper\index{pepper!marigold} (Piper marginatum), a pepper native to parts of South America, does have some hemostatic properties, hence it's nickname the ``soldier's herb.''\supercite{marigold-pepper}
All of these considerations aside, any pepper will cause a burning sensation in wounds and for that reason alone it should be avoided.
If you are considering using a hemostatic agent, avoid herbal remedies and use a commercially produced medical product.

\index{paraherbalism!wound management@in wound management|)}
\index{urban legends!wound management@in wound management|)}
\index{pepper!urban legends@in urban legends|)}
\index{urban legends!pepper@in pepper|)}
\index{wounds!urban legends@in urban legends|)}

\section*{Summary}

Treating closed wounds is done with ice and time.
Open wounds are treated by stopping bleeding with gauze and pressure, cleaning the wound, then dressing the wound.
Severe bleeding may require use of hemostatic gauze or a tourniquet.
Use of a tourniquet always requires evacuation to advanced medical care.
Patients can lose what appears to be a massive amount of blood before being at risk for hypovolemic shock.
Attempting to estimate this can help you make a decision on whether or not they need to be evacuated to advanced medical care or simply sent home.

% TODO this chapter should probably cover infection (identifying, recommendations for advanced medical care, etc)

\index{wounds|)}
