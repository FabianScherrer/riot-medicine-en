\chapter{Introduction}

\epigraph{
I can also tell in whose hands I am.
Do these hands tremble?
There can be no doubt: these are the hands of a military officer.
Is it a firm pulse?
I say without vacillating: these are the hands of a liberator.
}{Ricardo Flores Magón, \textit{The Rifle}\supercite{magon-reader}}

\noindent
Riot medicine is the practice of medicine in an adversarial environment.
It exists outside of formal and State sanctioned medical services.
Practitioners of riot medicine go by many names (riot medics, street medics, demonstration medics, action medical), but at the end of the day, their goals are the same.
They take to the streets as part of the diverse system of mutual aid that allows individuals to engage in protest.
The duties of a riot medic may include handing out water during a peaceful demonstration, providing late-night jail support for arrested comrades, caring for injured protesters and bystanders during a riot, or extracting and providing lifesaving interventions for combatants during an armed uprising.

The lens of riot medicine rather than street medicine was chose to help you focus more on how to provide medical care during demonstrations and physical engagements rather than to inform you on how to run a volunteer clinic or provide care for injuries sustained outside of short lived confrontations.\footnotemark[1]
The aim is to provide enough medical and tactical knowledge to enable riot medics to support short mobilizations on the scale of several hours to several days.

\footnotetext[1]{
Riot medics are strongly encouraged to apply their medical knowledge by providing aid in other contexts such as caring for houseless persons and providing disaster relief.
}

If you are an experienced medical professional, this book will guide you on how to safely operate during a protest.
However, this book assumes that medicine may not be your primary occupation or field of study.
Both the common and more formal medical terms are included as well as a glossary for reference.
Foundational medical theory has been provided to give context for various treatments, and as such, not all information in this book needs to be memorized.
Some information may seem obvious, but what is obvious to you is not obvious to others.
In depth information is provided to help demystify seemingly esoteric practices and address common misconceptions.

Because of the exceptionally diverse conditions under which riot medicine is practiced, this book generally avoids making absolute statements about how an individual or group must act.
Riot medics may be part of the Black Bloc or may act as seemingly neutral third parties.
They may be uncertified or may be practicing physicians.
How they choose to act depends on may factors including the nature of the \gls{action}, the legality of protest, the legality of practicing medicine, and the overall political climate of the region where an \gls{action} is taking place.
This book will provide you with a toolbox that will help you make operational decisions using your own experiences and context specific information.

Riot medicine incorporates elements of wilderness medicine and combat medicine, but it is still a distinct practice.
Often the riot medic is only equipped with what they can carry in a backpack.
What they choose to pack is limited by multiple factors, the major one being that their gear can be confiscated or destroyed during the course of their work.
They need to carry provisions to survive the day and personal protective equipment to keep themselves safe enough to do their job.
The riot medic needs to take a highly practical approach to medicine knowing that they will not be able to operate under ideal conditions.
Hospital-quality diagnostic equipment will not be available, materials may be limited, and care rendered often will only be ``good enough'' to get a restless comrade back into the fray.

Riot medics comfort traumatized comrades as much as they heal their bodies.
Protests and confrontations with fascists and the State can be stressful and even traumatizing.
Even in the nonideal environments you will be working in, it is your responsibility to keep calm and help calm those around you.
Nervous and stressed out comrades can be liable to make mistakes that lead to more injuries.
Reading this book will help enable you to act confidently and therefore help others act confidently, contributing toward successful demonstrations and insurrections.

This book is written from an autonomous, anarchist perspective.
However, the information and tactics described within will be useful to all participants in the struggle for liberation.
State imposed laws and regulations are a reality, and where it is relevant, it is noted where your work may intersect with the legal system to highlight what legal risks there may be.
This book was written in 2019--20, so as you are reading this, be wary that medical best practices, legal considerations, and all other information may have become out of date.

The act of challenging the State is dangerous, but with some basic knowledge, medics can drastically reduce the repercussions protesters face.
The goal is that by reading this book, you will be able to provide care for and support to comrades known and unknown, all in the pursuit of a world free of domination.
